openapi: 3.1.0
info:
  contact:
    email: it@openpass.com.ar
  title: Transacciones Complementarias.
  description: Transacciones Complementarias utilizadas en la plataforma BHub Fintch API. estas operaciones se vinculan a la transacción original como transacciones anexadas, que son configurables desde la plataforma
  version: '1.0'
  license:
    name: Openpass©
    url: 'https://www.openpass.com.ar'
servers:
  - url: 'https://api.dev.openpass.com.ar/bhubApi/1.0.0'
paths:
  '/transaction/{transaction_Id}/complementary-transaction/cash-debit':
    post:
      operationId: transaction-id-complementarytransaction-cashdebit
      parameters:
        - in: path
          name: transaction_Id
          required: true
          description: En este parámetro se introduce el ID de la transacción a la cual se desea crear una transacción complementaria.
          schema:
            type: string
          example: 6EEC04E0038DC8041674B37A0053C58F975C
      responses:
        '200':
          content:
            application/json:
              schema:
                $ref: ../models/Cashback/TransaccionReintegro.yaml
          description: OK
        '401':
          content:
            application/json:
              schema:
                $ref: ../models/GvpError.yaml
          description: Unauthorized
        '404':
          content:
            application/json:
              schema:
                $ref: ../models/GvpError.yaml
          description: The specified resource was not found
        default:
          content:
            application/json:
              schema:
                $ref: ../models/GvpError.yaml
          description: Unexpected error
      requestBody:
        content:
          application/json:
            schema:
              $ref: ../models/Cashback/TransaccionReintegro.yaml
      description: Este método API adapta el servicio de creación de transacciones complementarias de débito asociada a una transacción dada.
      summary: Crear transacción complementaria.
      tags:
        - Cash Debit
      x-stoplight:
        id: xnkkikygh1n1x
  /transaction/transaction-reasons:
    get:
      operationId: get-transaction-transactionreasons-transactiontype-cashbacks
      parameters:
        - schema:
            type: string
            example: cashback
          in: query
          name: transactionType
          description: Tipo de Transacción que se desea buscar en la lista para obtener el ID
      responses:
        '200':
          content:
            application/json:
              schema:
                type: array
                items:
                  $ref: ../models/Transaction/TransactionReason.yaml
          description: OK
        '401':
          content:
            application/json:
              schema:
                $ref: ../models/GvpError.yaml
          description: Unauthorized
        '404':
          content:
            application/json:
              schema:
                $ref: ../models/GvpError.yaml
          description: The specified resource was not found
        default:
          content:
            application/json:
              schema:
                $ref: ../models/GvpError.yaml
          description: Unexpected error
      description: Este método se utiliza para verificar los motivos tipificados para calificar un CashBack. Los motivos se definen y especifican desde la plataforma RUI en el apartado “Motivos de Transferencia”. 
      summary: Obtener Motivos de Cashback
      tags:
        - Cash Back
    parameters: []
  '/transaction/{transactionId}/complementary-transaction/cash-back':
    post:
      operationId: post-transaction-Id-complementarytransaction-cashback
      parameters:
        - in: path
          name: transactionId
          required: true
          example: 2D06054502C73EE92C517FF3005571113F15
          description: En este parametro se ingresa el ID de la transaccion a le queremos realizar un CashBack.
          schema:
            type: string
      responses:
        '200':
          content:
            application/json:
              schema:
                $ref: ../models/Cashback/TransaccionReintegro.yaml
          description: OK
        '401':
          content:
            application/json:
              schema:
                $ref: ../models/GvpError.yaml
          description: Unauthorized
        '404':
          content:
            application/json:
              schema:
                $ref: ../models/GvpError.yaml
          description: The specified resource was not found
        default:
          content:
            application/json:
              schema:
                $ref: ../models/GvpError.yaml
          description: Unexpected error
      requestBody:
        content:
          application/json:
            schema:
              $ref: ../models/Cashback/TransaccionReintegro.yaml
      description: 'Este método se utiliza para crear un CashBack asociado a una transacción específica. La transacción debe configurarse de tal manera que pueda ser susceptible de aplicar un CashBack. Los reembolsos se pueden aplicar inmediatamente o se pueden definir para que se apliquen en el futuro si se establece una fecha hábil. En este último caso, cuando un Cashback tiene fecha de acreditación futura, el monto del CashBack permanece dentro de una cuenta de custodia pendiente de acreditar. Posteriormente, el motor de negocios de Bhub se encarga de certificar el reembolso en la fecha de vencimiento trasladando el monto del CashBack a la cuenta disponible para que pueda ser utilizado por el cliente.'
      summary: Crear Cashback
      tags:
        - Cash Back
      x-stoplight:
        id: t4f1jup7vhckc

    get:
      operationId: get-transaction-id-complementarytransaction-cashback-clientTransactionId
      parameters:
        - in: path
          name: transactionId
          required: true
          description: En este parametro de ruta se ingresa el ID de la transaccion la cual queremos consultar.
          schema:
            type: string
          example: 567B054502C9D17B68ECED010055F8652BF2
      responses:
        '200':
          content:
            application/json:
              schema:
                $ref: ../models/Cashback/TransaccionReintegro.yaml
          description: OK
        '401':
          content:
            application/json:
              schema:
                $ref: ../models/GvpError.yaml
          description: Unauthorized
        '404':
          content:
            application/json:
              schema:
                $ref: ../models/GvpError.yaml
          description: The specified resource was not found
        default:
          content:
            application/json:
              schema:
                $ref: ../models/GvpError.yaml
          description: Unexpected error
      description: "Metodo para buscar una operacion de cashback desde su id de creacion"
      summary: Obtener Transacción del Cashback
      tags:
        - Cash Back
      x-stoplight:
        id: hezp0pbbbiwcx
  '/transaction/{{cashback_Id}}/cancel':
    post:
      operationId: post-transaction-id-cancel
      parameters:
        - in: path
          name: cashback_Id
          description: En este parametro de ruta se ingresa el ID de la Transacción del Cashback.
          required: true
          schema:
            type: string
          example: 567B054502C9D17B68ECED010055F8652BF2
      responses:
        '200':
          content:
            application/json:
              schema:
                $ref: ../models/Cashback/TransaccionReintegro.yaml
          description: OK
        '401':
          content:
            application/json:
              schema:
                $ref: ../models/GvpError.yaml
          description: Unauthorized
        '404':
          content:
            application/json:
              schema:
                $ref: ../models/GvpError.yaml
          description: The specified resource was not found
        default:
          content:
            application/json:
              schema:
                $ref: ../models/GvpError.yaml
          description: Unexpected error
      description: "método que permite cancelar un cashback que fue acreditado anteriormente, utilizando su id de creación para anularlo, permite retirarle el monto que le fue acreditado aneriormente"
      summary: Cancelar Cashback
      tags:
        - Cash Back
      x-stoplight:
        id: li87fctmfuuek
tags:
  - name: Cash Debit
    description: Request de Cashback
  - name: Cash Back
    description: Request de Cashback
components:
  securitySchemes:
    deviceId:
      name: deviceId
      type: apiKey
      in: header
      description: identificador
    applicationId:
      name: applicationId
      type: apiKey
      in: header
      description: identificador en el arbol comercial
    accessToken:
      name: accessToken
      type: apiKey
      in: header
      description: Token de acceso con permisos configurados para el uso requerido
security:
  - deviceId: []
    applicationId: []
    accessToken: []
