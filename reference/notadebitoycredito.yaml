openapi: 3.1.0
info:
  contact:
    email: it@openpass.com.ar
  title: Ajustes Credito y Debito
  description: BHub Fintch API Debit and Credit Note Module
  version: '1.0'
  license:
    name: Openpass©
    url: 'https://www.openpass.com.ar'
servers:
  - url: 'https://dev.openpass.com.ar/bhubApi/1.0.0'
paths:
  '/treasury/adjustments/{account_id}/currency/debit/availables-financial-accounts':
    get:
      operationId: get-treasury-adjustments-account_id-currency-debit-availablefinancialaccounts
      parameters:
        - in: path
          name: account_id
          required: true
          description: En este parámetro se introduce el ID de la cuenta a la cual se desea realizar un débito de fondos. El account_id se obtiene del campo ID del endpoint https://dev.openpass.com.ar/bhubApi/1.0.0/my-account 'https://docs.openpass.com.ar/docs/bhub-openpass-api/79fa31bea6cdb-consulta-de-cuenta'.
          example: 103A054D0475132C179BB1CE00549E8060B2
          schema:
            type: string
      responses:
        '200':
          content:
            application/json:
              schema:
                $ref: ../models/Money/DebitOrigin.yaml
          description: OK
        '401':
          content:
            application/json:
              schema:
                $ref: ../models/GvpError.yaml
          description: Unauthorized
        '404':
          content:
            application/json:
              schema:
                $ref: ../models/GvpError.yaml
          description: The specified resource was not found
        default:
          content:
            application/json:
              schema:
                $ref: ../models/GvpError.yaml
          description: Unexpected error
      description: Este método permite obtener una lista de cuentas financieras que funcionarán como origen y destino de los fondos para realizar una nota de débito. Para lograrlo, debes ingresar el ID de la cuenta de la cual se quieren debitar los fondos. Este método devolverá la cuenta financiera de origen (la cuenta financiera asociada al ID ingresado en el parámetro de ruta) y una lista con las posibles cuentas que servirán como destino (donde se acreditará el dinero)
      summary: Consulta de Origen y Destino para Nota de Débito
      tags:
        - Nota de Débito
  '/treasury/adjustments/{account_id}/currency/debit':
    post:
      operationId: post-treasury-adjustments-account_id-currency-debit
      parameters:
        - in: path
          name: account_id
          required: true
          example: 103A055303F94C24047BAA79005671C2AA29
          description: En este parámetro se introduce el ID de la cuenta a la cual se desea realizar un débito de fondos. El account_id se obtiene del campo ID del endpoint https://dev.openpass.com.ar/bhubApi/1.0.0/my-account 'https://docs.openpass.com.ar/docs/bhub-openpass-api/79fa31bea6cdb-consulta-de-cuenta'.
          schema:
            type: string
      responses:
        '200':
          content:
            application/json:
              schema:
                $ref: ../models/Money/NotaDebitoResponse.yaml
          description: OK
        '401':
          content:
            application/json:
              schema:
                $ref: ../models/GvpError.yaml
          description: Unauthorized
        '404':
          content:
            application/json:
              schema:
                $ref: ../models/GvpError.yaml
          description: The specified resource was not found
        default:
          content:
            application/json:
              schema:
                $ref: ../models/GvpError.yaml
          description: Unexpected error
      requestBody:
        content:
          application/json:
            schema:
              $ref: ../models/Money/NotaDebitoRequest.yaml
      description: Este método brinda la posibilidad de generar una nota de débito para una cuenta financiera específica. Los datos de origen y destino se pueden consultar utilizando el siguiente método GET/treasury/adjustments/{account_id}/currency/debit/availables-financial-accounts
      summary: Crear Nota de Débito
      tags:
        - Nota de Débito
  '/treasury/adjustments/{account_id}/currency/credit/availables-financial-accounts':
    parameters:
        - in: path
          name: account_id
          required: true
          description: En este parámetro se introduce el ID de la cuenta a la cual se desea realizar un credito de fondos. El account_id se obtiene del campo ID del endpoint https://dev.openpass.com.ar/bhubApi/1.0.0/my-account 'https://docs.openpass.com.ar/docs/bhub-openpass-api/79fa31bea6cdb-consulta-de-cuenta'.
          example: 103A054D0475132C179BB1CE00549E8060B2
          schema:
            type: string
    get:
      summary: Consulta de Origen y Destino para Nota de Crédito
      responses:
        '200':
          description: OK
          content:
            application/json:
              schema:
                $ref: ../models/Money/CreditOrigin.yaml
        '401':
          description: Unauthorized
        '403':
          description: Forbidden
          content:
            application/json:
              schema:
                type: object
                properties: {}
      operationId: get-treasury-adjustments-account_id-currency-credit-availables-financial-accounts
      description: Este método permite obtener una lista de cuentas financieras que funcionarán como origen y destino de los fondos para realizar una nota de crédito. Para lograrlo, debes ingresar el ID de la cuenta a la cual se le quieren acreditar los fondos. Este método devolverá la cuenta financiera de destino (la cuenta financiera asociada al ID ingresado en el parámetro de ruta) y una lista con las posibles cuentas que servirán como origen (de donde se debitará el dinero).
      tags:
        - Nota de Crédito
  '/treasury/adjustments/{account_id}/currency/credit':
    parameters:
      - schema:
          type: string
          example: '103A055302E9655EE131ED5E0055A50CFE86'
        name: account_id
        in: path
        required: true
        description: En este parámetro se introduce el ID de la cuenta a la cual se desea realizar un credito de fondos. El account_id se obtiene del campo ID del endpoint https://dev.openpass.com.ar/bhubApi/1.0.0/my-account 'https://docs.openpass.com.ar/docs/bhub-openpass-api/79fa31bea6cdb-consulta-de-cuenta'.
    post:
      summary: Crear Nota de Crédito
      operationId: post-treasury-adjustments-currency-credit
      responses:
        '200':
          description: OK
          content:
            application/json:
              schema:
                $ref: ../models/Money/NoteResponse.yaml
        '401':
          description: Unauthorized
        '403':
          description: Forbidden
      description: Este método brinda la posibilidad de generar una nota de crédito para una cuenta financiera específica. Los datos de destino y origen se pueden consultar utilizando el siguiente método; GET/treasury/adjustments/{account_id}/currency/credit/availables-financial-accounts
      requestBody:
        content:
          application/json:
            schema:
              $ref: ../models/Money/NoteRequestCredit.yaml
      tags:
        - Nota de Crédito
  '/treasury/adjustments':
    get:
      operationId: get-trasury-adjustments
      parameters:
        - in: query
          name: endDate
          required: false
          schema:
            type: string
        - in: query
          name: beginDate
          required: false
          schema:
            type: string
        - in: query
          name: state
          required: false
          schema:
            type: string 
        - in: query
          name: clientTransactionId
          required: false
          schema:
            type: string
        - in: query
          name: startIndex
          required: false
          schema:
            type: number
        - in: query
          name: limitIndex
          required: false
          schema:
            type: number
        - in: query
          name: transactionNumber
          required: false
          schema:
            type: number
        - in: query
          name: productId
          required: false
          schema:
            type: string
      responses:
        '200':
          content:
            application/json:
              schema:
                type: array
                items:
                  $ref: ../models/Money/NotaDebitoResponse.yaml
          description: OK
        '401':
          content:
            application/json:
              schema:
                $ref: ../models/GvpError.yaml
          description: Unauthorized
        '404':
          content:
            application/json:
              schema:
                $ref: ../models/GvpError.yaml
          description: The specified resource was not found
        default:
          content:
            application/json:
              schema:
                $ref: ../models/GvpError.yaml
          description: Unexpected error
      description: "Permite consultar notas de debito y credito asociados al accessToken el usuario que lo ejecuta"
      summary: Obtener Notas de Crédito/Débito
      tags:
        - Consultar Notas de Crédito y Débito
tags:
  - name: Nota de Crédito
    description: Request de Core
  - name: Nota de Débito
    description: Request de Core
  - name: Consultar Notas de Crédito y Débito
    description: Request de Core
components:
  securitySchemes:
    deviceId:
      name: deviceId
      type: apiKey
      in: header
      description: identificador
    applicationId:
      name: applicationId
      type: apiKey
      in: header
      description: identificador en el arbol comercial
    accessToken:
      name: accessToken
      type: apiKey
      in: header
      description: Token de acceso con permisos configurados para el uso requerido
security:
  - deviceId: []
    applicationId: []
    accessToken: []
