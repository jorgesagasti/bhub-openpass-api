# Soporte de Integración

OpenPass ofrece un servicio de Soporte 24x7 para Integraciones con el siguiente alcance:

- Lunes a Viernes de 9 a 18 horas: Soporte Técnico, Atención Comercial y Soporte Transaccional.

### Canales de Atención

- Implementaciones: integraciones@openpass.com.ar

- Soporte Técnico: soporte@openpass.com.ar

- Otras consultas: info@openpass.com.ar