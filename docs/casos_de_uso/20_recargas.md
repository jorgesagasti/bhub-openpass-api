# Recargas

## Introducción

El presente documento describe el uso de las APIs para poder realizar distintos tipos de Recargas desde la Plataforma OpenPass. 

La Plataforma está conectada con diversos proveedores de servicios de pago, y cada uno de ellos cuenta con distintos procesos, servicios, flujos y circuitos para llevar a cabo sus operaciones. Lo que se busca es unificar y consolidar las diferencias de cada proveedor y ofrecer servicios propios de la Plataforma, con el fin de facilitar y agilizar el flujo de operaciones para el cliente.

Las diferencias entre proveedores son resueltas internamente por la Plataforma, la misma Plataforma es la encargada de resolver la manera de interactuar con cada proveedor según esté definido en las reglas de negocio.

Debido a que la Plataforma OpenPass se encuentra en constante evolución, ya sea por mejoras incorporadas, nuevas funcionalidades, cambios normativos y/o cambios en los proveedores y con el afán de lograr una documentación actualizada, los detalles técnicos de cada uno de los métodos y sus atributos se encuentran publicados en formato OpenAPI en este mismo sitio. Por ese mismo motivo, no generamos documentos estáticos.

 
### Detalle de métodos de Recargas:

* [POST /product](../../reference/recargas.yaml/paths/~1products/post)

* [GET /products](../../reference/recargas.yaml/paths/~1products/get)

* [GET /product/id](../../reference/recargas.yaml/paths/~1product~1{id}/get)

* [PUT /product/id](../../reference/recargas.yaml/paths/~1product~1{id}/put)

* [POST /sales/topup/simulate](../../reference/recargas.yaml/paths/~1sales~1topup~1simulate)

* [POST/sales/topup](../../reference/recargas.yaml/paths/~1sales~1topup/post)

* [GET/sales/id/receipt](../../reference/recargas.yaml/paths/~1sale~1{id}~1receipt/get)

* [GET/sales/](../../reference/recargas.yaml/paths/~1sales/get)

* [GET /sales/id](../../reference/recargas.yaml/paths/~1sale~1{id})

* [GET /sales/salesCalendar](../../reference/recargas.yaml/paths/~1sales~1salesCalendar/get)

## Recargas

Dentro del grupo de recargas se puede encontrar cualquier tipo de Producto prepago previamente definido, entre ellos:

 * Recarga Telefónica. <!-- Son empresas como Claro, Personal, Movistar, tuenti -->

 * Recarga TV Satelital. <!--Es la empresa direct TV-->

 * Recarga Transporte. <!--Es la empresa Sube-->

 * Etc. 

Como se mencionó anteriormente, la Plataforma tiene como objetivo unificar los servicios de los distintos proveedores para así, con un único método, tener la posibilidad de realizar los distintos tipos de Recargas.

El punto de diferenciación entre los distintos tipos de Recarga, son los datos necesarios para poder realizarla. Estos datos son los que deben estar incluidos en el atributo [_TransactionData_](../../models/Transaction/TransactionData/TransactionData.yaml).

#### **_GET/products_**

Mediante este método se obtiene una lista de los productos disponibles en la Plataforma para poder efectuar una Recarga.

Dentro del grupo de Recargas, _GET/products_ es el primer método al que se debe llamar, ya que para poder realizar una Recarga es necesario previamente identificar el código del producto para el que se quiere realizar la recarga.

Dentro de la lista de productos que se obtendrán a partir de este método, cada uno de ellos pertenece a un grupo de productos entre los cuales se encuentran Recargas Telefónicas, Recargas de TV Satelital, Recarga de Transporte, etc.

#### **_POST/sales/topup_**

Este es el método mediante el cual se realizan transacciones de Recarga.

Una vez obtenido e identificado el Producto que se quiere recargar, se debe utilizar el "id" del producto para llamar al método.

Dentro del request body, los datos necesarios para realizar la recarga son los siguientes:

* transactionType: este campo espera en este caso el dato "Sale" lo que identifica la operacion como una recarga.

* amount: La cantidad de dinero que se desea recargar. Es importante tener en cuenta que existe una cantidad mínima y una cantidad máxima de dinero a recargar, estos límites son fijados por el cliente en la carga del nuevo producto.

* clientTransactionId: ID de transacción del Cliente, es un atributo que envía el cliente. Es importante ya que en caso de no recibir respuesta al realizar una transacción, puede ser utilizado para encontrar la operacion en la plataforma y verificar el estado de la misma.

* product: (atributo requerida: id): Objeto que contiene el "id" del producto a recargar. Este "id" se obtiene a través del endpoint GET /products.

* transactionData: es una estructura de datos que varía según el tipo de transacción. Contiene la información necesaria para ejecutar la operación, y su formato específico se define mediante el atributo "_transactionData.ClassName_" del producto. Por ejemplo, en una recarga telefónica, "_transactionData.ClassName_" es "PhoneData".

**Ejemplo de recarga telefónica**: 

Para recarga telefónica se debe enviar un objeto PhoneData del tipo transactionData, que contiene el número de teléfono que se quiere recargar y se envía en el siguiente formato:

    Se informa el país, el código de área, sin incluir el 0 y el número de línea. (Si no se informa el prefijo de país, se asume el código de país determinado por el cliente) 

        Ej 1: Área 11 y número 12345678: se informa: transactionData.phoneNumber: '1112345678' 

        Ej 2: Área 2965 y número 123456: se informa: transactionData.phoneNumber: '2965123456' 

        Ej 3: código de país: +54, Área 341 y número 6123456: se informa: transactionData.countryCode: '+54' transactionData.phoneNumber: '3416123456'

Lo que se utiliza como validación en este caso es que la cantidad de dígitos ingresados por el usuario sea igual a diez.    

**Ejemplo de recarga por Código**:  

Se debe enviar un objeto CodeData del tipo transactionData. Lo usan las empresas que utilizan un código relacionado al abonado para identificar la recarga. La cantidad de dígitos varía dependiendo de la empresa a la cual se desea recargar y es lo que se utiliza para verificar que los datos ingresados por el usuario sean válidos. 

    Para la venta de estos productos se debe informar el código del abonado sin ningún tipo de formato. 


        Ejemplo para DirectV: 

        123456789123-456789: se informa: transactionData.code: '123456789123456789' 

        
        Ejemplo para SUBE Diferido: 

        1234567891234567: se informa: transactionData: 1234567891234567 (corresponde al nro. de tarjeta de 16 dígitos) 


#### _**GET/product/id**_

Este método se utiliza para consultar sobre un producto específico. Conociendo el código de identificación del producto que desea consultar, se llama al método y se obtienen todos los detalles de dicho producto.

#### _**GET/sale/id/receipt**_

Este método se utiliza para obtener el ticket de una transacción. Lo primero que hay que hacer es identificar el "id" de la transacción, eso se obtiene como respuesta del método _POST/sales/topup_ o consultando mediante el _clientTransactionId_. 
Se obtiene un ticket en donde se muestra la fecha, la hora y el importe de la transacción, el tipo de recarga, el estado de la recarga (confirmada, pendiente o error), etc.

#### _**GET/sales/?clientTransactionId={{clienttransactionId}}**_

Cuando el usuario realiza una transacción y no recibe respuesta del sistema, se utiliza el _ClientTransactionId_ para buscar e identificar el estado final de la transacción.

 <span style="text-decoration:underline;">**Consideraciones**</span>

> Se debe enviar el _clientTransactionId_ correspondiente a la transaccion que se desea consultar. De esta manera se podrá utilizar para verificar la existencia y el estado de la transacción dentro de la Plataforma OpenPass.

#### Herramientas de testing

> En los ambientes de testing las respuestas a las recargas varían según la terminación de los últimos 4 dígitos enviados en el _transactionData_:

En el estado de la transacción, se puede observar:

> - Menores a 1000 ("state": "approved")
> - Entre 1000 y 2000 ("state": "pending) (Cuanto mas alto el numero, mayor es el tiempo que Bhub espera en responderte)
> - Mayor a 2000 ("state": "rejected)

Ejemplo:
Para el caso de una transacción de recarga del tipo recarga telefónica, los datos a enviar son:

objeto PhoneData, en el atributo PhoneNumber

+543416120123 -> "0123" -> "state": "approved"

+543416121500 -> "1500" -> "state": "pending"

+543416129999 -> "9999" -> "state": "rejected"


#### Errores posibles de una Transacción

Al igual que el resto de las transacciones gestionadas por la Plataforma OpenPass, los criterio utilizados para el Tratamiento de Errores corresponden a los detallados en la sección [**_<span style="text-decoration:underline;">"Tratamiento de Errores Plataforma OpenPass"</span>_**](../Errores/tratamiento_de_errores.md)


# _Top-ups_

## _Introduction_

_This document describes the use of APIs to perform different types of Top-ups from the OpenPass Platform._

_The Platform is connected to various payment service providers, and each of them has different processes, services, flows and circuits to carry out their operations. The aim of the Platform is to unify and consolidate the differences of each provider and offer services to facilitate and streamline the flow of operations for the client._

_Differences between suppliers are resolved internally by the Platform, which is responsible for finding the way to interact with each supplier as defined in the business rules._

_Due to the OpenPass Platform being in constant evolution, either because of enhancements, new features, changes in https://docs.openpass.com.ar/docs/bhub-openpass-api/b3d865012ca13-servicios-bancariosregulations and/or changes in the providers, and aiming to achieve up-to-date documentation, technical details of each method and their attributes are published in an OpenAPI format on this website. Because of this, we do not generate static documents._

### _Detail of Top-up methods:_

- _GET / products which returns a list of Products_
- _POST/sales/topup_
- _GET/products/id_
-	_GET/sales/id/receipt._
-	_GET/sales/?clientTransactionId={{clienttransactionId}}._

## _Top-ups_

_Within the top-ups group you can find different types of previously defined pre-paid products:_

-	_Phone Top-up_
-	_Satellite TV Top-up_
-	_Transport top-up_
-	_Etc._

_As mentioned earlier, the Platform's aim is to unify the services of different providers so that there is a single method and there is a possibility to perform different types of Top-ups._

_The point of differentiation between the different types of Top-ups is the data necessary to perform it. This data should be included in the **Transaction Data attribute** ._

#### _GET/products_


_This method is used to get a list of available products in the Platform to perform a top-up._

_Within the Top-up group, GET/products is the first method to be called since it is necessary to previously identify the code of the product from which you want to recharge._

_The list of products obtained by this method includes a group of products among which you can find telephone top-ups, satellite TV top-ups, transport top-ups, etc._

#### _POST/sales/topup_

_This is the method by which top-up transactions are made._

_Once you have obtained and identified the product to be recharged, you must use the product code to call the method._

_Within the request, the necessary data to perform the recharge are the following:_

-	_Transaction Type: Sale_
-	_Amount: The amount of money you want to top up. It is important to know that there is a minimum amount and a maximum amount of money to top up. These limits are set by the company._
-	_Product Id: The identification of the product that is reloaded. (This is the code that was previously obtained with the GET/products method.)_
-	_Transaction Data: It is a validation attribute that is needed to verify the information provided by the user. This verification is performed by the provider. Transaction Data is an attribute that changes depending on the Transactiondataclass of the product being recharged. The Transactiondataclass will depend on the product in question; for example, if a Telephone Top-up is being made, it would be Phone Data._

_**Phone Top-up**: For phone top-ups, the Transaction Data attribute is the phone number and is presented as follows:_

    Country, area code (not including 0) and phone number are reported. (If the country code is not reported, it is assumed to be +54, Argentina)

    E.g., 1: Area 011 and number 12345678: it is reported: transactionData: 1112345678

    E.g., 2: Area 02965 and number 123456: it is reported: transactionData: 2965123456

_What is used as validation, in this case, is the number of digits entered by the user, which is equal to ten._

_**Top-ups per Document**: Used when you want to top-up products that need the document type and number._

_The document type must be specified:_

-	_ID = 1_
-	_Personal Identity Booklet = 2_

_Followed by the document number_

    For example:

    - _ID 12,345,678: it is reported: transactionData: 112345678_
    - _Personal Identity Booklet 123.456: it is reported: transactionData: 3123456_

_**Top-ups by Code**:_ 

_Used by companies that use a subscriber-related code to identify the Top-Up. The number of digits varies depending on the company to which you want to top up and is used to verify that the data entered by the user is valid._

    For the sale of these products, the subscriber's code must be reported without any format.

    Example for DirecTV:
    123456789123-456789:  is reported: transactionData: 123456789123456789


    Example for Edenor Prepaid Service:
    07012345678:  is reported: transactionData: 07012345678 (corresponds to n° of indicator)


    Example for Deferred SUBE:
    1234567891234567: se informa: transactionData: 1234567891234567 (corresponds to a 16-digit card number)

_The last required data within the request is the Client Transaction Id, which is an attribute sent by the user. It is important because, in case of no response when making a transaction, it can be used to check the status of the transaction._

#### _GET/products/id_

_This method is used to inquire about a specific product. When knowing the identification code of the Product you want to consult, the method is called and all the details of that Product are obtained._

#### _GET/sales/id/receipt_

_This method is used to get the ticket for a transaction. The first thing to do is to identify the transaction that is obtained as a response from the method POST / sales / topup._


_That Transaction Id is what is used to search for the proof of a specific transaction. You get a ticket showing the date, time and amount of the transaction, the type of recharge, the status of the recharge (confirmed, pending, or error), etc._

#### _GET/sales/?clientTransactionId={{clienttransactionId}}._

_This method describes the importance of the attribute ClientTransactionId._

_When the user makes a top-up and receives no response from the system, it is necessary to use ClientTransactionId to search for and identify the status of the transaction._

 <span style="text-decoration:underline;">**_Considerations_**</span>:

> _In the attribute clientTransactionId the Transaction ID of the caller must be sent. Thus, the method can be used to identify the Transaction within the OpenPass Platform when necessary._

> _For example, when the communication is cut in the response of the initial call. In this case, it will not be possible to know if the transaction was successful or not as there was no response and therefore it was not obtained in  **confirmationId**._
>
> _In this case, the status of the Transaction must be consulted using the method **GET/sale / {id}**  where id = clientTransactionId_

> _In testing environments, the responses to top-ups vary according to the completion of the last 4 digits entered:_
> -	_Under 1000 (OK Answer)_
> -	_Between 1000 and 2000 (Returns pending) (The higher the number, the longer Bhub waits to answer you)_
> -	_Greater than 2000 (Simulation of various errors)_

> _**possible response statuses of a Transaction**_
>
> _Like the rest of the transactions managed by the OpenPass Platform, the criteria used for the Treatment of Errors corresponds to that detailed in the section **_<span style="text-decoration:underline;">"Error Handling OpenPass Platform"</span>_**_