# Tarjetas

### Diagrama de Entidad Relación del modelo de Tarjetas


El siguiente esquema describe el Diagrama de Entidad Relación del modelo de Tarjetas de la Plataforma OpenPass.


El diagrama no incluye detalles de los datos y atributos de cada entidad, solo es una visión general para lograr una mejor comprensión del modelo y que esto resulte de utilidad para el uso de los métodos de la API para la gestión de tarjetas, cuentas-tarjetas, etc.

![Diagrama de Entidad Relación del modelo de Tarjetas](../../assets/images/DER_TarjetasV2.png)



A continuación se describe el uso de los métodos de APIs de Cuentas Tarjetas y Tarjetas de la Plataforma OpenPass en todas sus variantes (Pre-paga, Crédito, Débito, Giftcard).


Se describen las consideraciones más relvantes para comprender y lograr una mejor utilización de los métodos de la API.


### Emisores de Tarjetas (Issuers)


Las cuentas tarjetas están siempre asociadas a un Emisor (Issuer) los cuales se crean previamente desde el BackOffice de la Plataforma OpenPass.


Una vez creados los Emisores con los que va a trabajar la Plataforma, se pueden consultar mediante los métodos de la API detallados en la sección "Issuers".

Esto es necesario para luego gestionar las Cuentas-Tarjetas y Tarjeras de cada individuo.

### Grupos de Afinidad


Así mismo, la Plataforma OpenPass permite generar diferentes Grupos de Afinidad (affinities) para cada Emisor (issuer). De esta manera se puede lograr una agrupación lógica de Cuentas Tarjeta dentro de cada Emisor en base a diferentes grupos de afinidad.


De la misma manera que los Emisores, los Grupos de Afinidad se crean previamente desde el BackOffice de la Plataforma OpenPass.


Una vez creados los Grupos de Affinidad con los que va a trabajar la Plataforma, se pueden consultar mediante los métodos de la API detallados en la sección "Affinity Groups".

### Cuentas Tarjetas (Card Accounts)


La Plataforma OpenPass maneja el concepto de Cuentas Tarjeta (Card Accounts) las cuales pueden estar o no asociadas a una Cuenta Billetera. Es decir, un usuario podría tener en la Plataforma OpenPass una o más Cuenta/s Financiera/s y una o más Cuentas Tarjetas, pero también podría tener solo una Cuenta Tarjeta y NO tener Cuentas Financieras.


Dentro de las Cuentas Tarjetas, se pueden diferenciar los siguientes tipos de tarjetas:

- Prepaga.
- Crédito.
- Débito.
- GiftCard.


La gestión de las Cuentas Tarjetas dentro de la Plataforma OpenPass se realiza mediante los siguientes métodos, los cuales se presentan a continuación agrupados de una manera lógica solo a modo de hacer más fácil su interpretación.



###  <span style="text-decoration: underline">Métodos para el Manejo de Cuentas-Tarjetas </span>

#### Alta de Cuentas Tarjeta Habiente:


Médiante los siguientes métodos se da de alta los diferentes tipos de CuentaTarjetaHabiente. Estos métodos generarán una cuenta del tipo correspondiente, asociada al Usuario y de manera automática, la primera vez, generará una Tarjeta del tipo correspondiente.

- POST/cardHolderAccount/prepaid

- POST/cardHolderAccount/credit

- POST/cardHolderAccount/debit

- POST/cardHolderAccount/giftcard

#### Alta de Cuenta Tarjeta Habiente Adicional:

Un caso particular de las cuentas tarjetas son aquellas que tiene relacionadas tarjetas adicionales. Las tarjetas adicionales comparten con las titulares algunas de sus características y también se diferencias en algunos puntos con aquellas.

La tarjeta adicional siempre existirá a partir de un cuenta tarjeta habiente TITULAR ACTIVA al momento de su creación.

La persona poseedora de una tarjeta adicional no tiene restricciones en nuestro modelo, en otras palabras el modelo de Bhub prevé que una misma persona puede tener múltiples adicionales de distintos titulares.

La persona poseedora de una tarjeta adicional podrá tener un domicilio de correspondencia diferente al domicilio declarado por el titular. El domicilio de correspondencia del adicional solo puede modificarse en nuestro modelo, mientras que del lado de la procesadora podría no existir funcionalidad para llevar a cabo actualización alguna.

Bhub valida que solo pueda existir una tarjeta adicional activa para un mismo DNI.

Para generar una tarjeta adicional se deberá usar el siguiente API Method

- POST/cardHolderAccount/{cardHolderAccountId}/additional


> **NOTA:** Si bien en el objeto que se envía, se solicita un _issuierId_ donde especifica ya el tipo de CuentaTarjetaHabiente a solicitar (prepaid, credit, debit, giftcard), en los llamados de API también se pide (por URL), para que a futuro no sea obligatorio pasar por un método de descubrimiento de _issuerId_, y la plataforma pueda calcularlo automáticamente.

#### Consulta de Cuentas Tarjeta:

Mediante el siguiente método se pueden consultar las CuentasTarjetaHabiente existentes. Este método devuelve un array de todos los CardHolderAccount activos y CardHolderAccountRequest en estado pendiente (ver descripción de "estado pendiente").

Un CardHolderAccount es un objeto que combina:
<b>CuentaTarjeta + Relacion_CuentaTarjeta + Persona + [Tarjetas] agrupándolos en un CuentaTarjetaHabiente</b>

- GET/cardHolderAccounts/ 

Mediante el siguiente método se pueden consultar una cuentaTarjetaHabiente en particular.

- GET/cardHolderAccount/{cardHolderAccountId}

Así mismo, se puede consultar por una cuentaTarjetaHabiente por el clientTransactionId enviado en la solicitud de Alta original en caso de ser necesario.

- GET/cardHolderAccount?clientTransactionId={value}

#### Cambio de Estados de una CuentaTarjetaHabiente

Mediante los siguientes métodos se puede cambiar el estado de una CuentaTarjetaHabiente en particular. Mediante estos métodos se puede ofrecer al usuario la administración y el control de sus Cuentas Tarjeta.

- POST/cardHolderAccount/{cardHolderAccountId}/action/activate

- POST/cardHolderAccount/{cardHolderAccountId}/action/deactivate

- POST/cardHolderAccount/{cardHolderAccountId}/action/disable

- POST/cardHolderAccount/{cardHolderAccountId}/action/freeze

- POST/cardHolderAccount/{cardHolderAccountId}/action/inhibit

#### Estado Pendiente:

Tal como ocurre con la mayoría de las transacciones, la Plataforma OpenPass considera y gestiona todos los posibles fallos y/o interrupciones en las integraciones con terceras partes (proveedores), en este caso los procesadores de Tarjetas. 

Estos posibles fallos y/o interrupciones pueden generar des-sincronización entre los diferentes objetos entre la Plataforma OpenPass y el proveedor, por ej. OpenPass puede NO recibir respuesta a un Alta de Tarjeta, con lo cual NO crearla, pero el proveedor SI la crea o viceversa. 

En este caso, la Plataforma OpenPass dejará el objeto CardHolderAccountRequest en estado "pendiente" para luego actualizar su estado final luego de sincronizarse con el proveedor y confirmar el estado real/final de la transacción mediante un proceso automatizado previamente. 

Es importante considerar que los CardHolderAccountRequest en estado pendiente, pueden confirmarse y convertirse en CardHolderAccount o anularse y desaparecer de esta consulta. 

Un caso particular es el referido a la solicitud de tarjetas adicionales debido a que la información suministrada por las procesadoras difieren de la solicitud de una cuenta titular. Es por este motivo que se determina el siguiente flujo para el tratamiento de las solicitudes de creación que han quedado en estado pendiente.

A partir del proceso de conciliación de alta de tarjetas cuando la procesadora devuelva adicionales que no existen en BHUB, esto aplica a adicionales des sincronizadas de las cuales no contamos con información suficiente de la persona, procederemos a dar de baja la tarjeta adicional debido a que no existe manera de completar los datos faltantes ni vincularlo con nuestro modelo. Este problema provoca entre otros no contar con la información necesaria para solicitar el embozado.


###  <span style="text-decoration: underline">Métodos para el Manejo de Tarjetas </span>

#### Consulta de Tarjetas

Mediante el siguiente método se permite consultar una Tarjeta en particular.

- GET /cardHolderAccount/card/{cardId} 

Este método, devuelve los datos completos de la tarjeta, conciliándose con un proveedor externo. 

Considerar que, si bien se están consultados objetos persistidos en la Plataforma OpenPass, la API puede devolver errores relacionados a la búsqueda externa de los datos sensibles de la tarjeta.

Se devolverán los datos sensibles planos o encriptados según corresponda, en distintos objetos (CardData o CardEncryptedData).

- GET /cardHolderAccount/{{cardHolderAccountId}}/additionals

Este método devuelve una colección con todas las tarjetas adiciones referidas a la cuenta titular.


###  <span style="text-decoration: underline">Regeneración de una Tarjeta </span>

Para realizar la Regeneración de una Tarjeta Prepaga se debe utilizar alguno de los siguientes  métodos. Se puede Regenerar tanto una tarjeta del mismo tipo (Virtual o Física) y de distinto tipo (de Virtual a Física o viceversa).

- POST /cardHolderAccount/card/{cardId}/regeneration/regenerate

Con el siguiente Request, se genera una nueva tarjeta, tanto de tipo física como virtual, dejando en estado "DeactivatedRenovation" lla previa del mismo tipo de si existiera una.

- POST /cardHolderAccount/{{cardHolderAccountId}}/card

###  <span style="text-decoration: underline">Modificación de Domicilio de Correspondencia </span>

Para realizar la modificación del domicilio de correspondencia de una CuentaTarjetaHabiente, al cual el usuario quiere que se le envíe la Tarjeta Prepaga Física que está solicitando, se debe utilizar el siguiente método. 

Esta información debe ser actualizada previo a solicitar el Alta o la Regeneración de una Tarjeta Física.

- PUT /cardHolderAccount/{cardHolderAccountID}/correspondence

###  <span style="text-decoration: underline">Modificación de Grupo de Afinidad </span>

Para realizar la modificación del Grupo de Afinidad de una Cuenta se debe utilizar el siguiente método:

- PUT /cardHolderAccount/cardHolderAccount/{cardHolderAccountID}

Esta información debe ser actualizada previo a solicitar el Alta o la Regeneración de una Tarjeta Física en caso que se esté utilizando el Grupo de Afinidad como agrupamiento lógico para por ej. tener tarjetas con diferentes diseños o diferentes categorías (ej: Gold, Platimun, etc).

###  <span style="text-decoration: underline">Habilitación de Tarjeta Física </span>

Luego de completar el flujo de Solicitud de Tarjeta Física mediante los llamados a los métodos de API correspondiente según cada caso, el proceso continúa ya por el circuito de logística de Embozado y entrega del plástico, el cual es independiente a la plataforma OpenPass. 

Al momento de ser recibida la Tarjeta por el Usuario, el mismo deberá habilitarla para lo cual se deberá utilizar el siguiente método.

- POST /cardHolderAccount/card/{cardId}/commercial-enable

Con este método se realiza la habilitación comercial de un objeto tarjeta. Devuelve el objeto tarjeta (card) utilizado. De manera automática, al solicitar la habilitación comercial de una tarjeta, se realizará la sincronización de existencia y estados de TODAS las tarjetas existentes para el Cuenta-TarjetaHabiente.

Se debe tener en cuenta que la tarjeta debe estar en estado "Packed"
 para poder habilitarla. Cuando la tarjeta se crea,  queda en estado "PendingPacked", cuando el proveedor la imprime, pasa automáticamente a estado "Packed"

> **NOTA:** Dado que, algunos proveedores (en caso de que el Usuario tenga previamente una tarjeta Virtual),  al momento de Habilitar la Tarjeta Física, da de baja la Virtual, en caso de querer mantener ambas tarjetas activas, luego de Habilitar la Física se DEBE cambiar el estado a la tarjeta Virtual, mediante el método correspondiente, para volver a Activarla.

- POST /cardHolderAccount/card/{cardId}/action/activate

###  <span style="text-decoration: underline">Cambio de Estados de una Tarjeta </span>

Mediante los siguientes métodos se puede cambiar el estado de una Tarjeta en particular. Mediante estos métodos se puede ofrecer al usuario la administración y el control de sus tarjetas.

Estados modificables desde el API

- Normal
- ReportTheft
- BlockedTemporal
- Deactivated

#### Casos de uso:

- "Normal", tarjeta en estado para ser usada normalmente

    - POST /cardHolderAccount/card/{cardId}/action/deactivate, pasa a estado "Deactivated"

    - POST /cardHolderAccount/card/{cardId}/action/lock-temporal, pasa a estado "BlockedTemporal"

    - POST /cardHolderAccount/card/{cardId}/action/activate, pasa a estado "Deactivated"

- De estado "Normal" a "ReportTheft". Reportar como robada.
    - POST /cardHolderAccount/card/{cardId}/action/report-theft, no puede volver a estado "Normal"


#### Consideraciones

- > El estado "Deactivated" y "BlockedTemporal" tienen el mismo efecto en el caso de las Tarjetas Prepaid, en ambos casos se rechazarán los consumos si la tarjeta se encuentra en cualquiera de los estados antes mencionados.

- > El método "/activate" es solo para revertir los estados "Deactivated", y "BlockedTemporal". Para "habilitar" una tarjeta física se debe utilizar el método descrito previamente.


####  <span style="text-decoration: underline">Estados no modificables desde el API, reportados por el proveedor</span>

- DeactivatedRenovation
- Pending
- Overdue
- Packed
- PendingPacked
- NotEnabledIvr
- RenovationlockedIvr
- DenouncedIvr
- Unknown

# _Cards_

### _Entity Diagram of Card model relationship_

_The following scheme describes the Relationship Entity Diagram of the OpenPass Platform Card model._
 
_The diagram does not include details of the data and attributes of each entity. It is only an overview to achieve a better understanding of the model so that it is useful for the use of the API methods for the management of cards, accounts-cards, etc._

![Diagrama de Entidad Relación del modelo de Tarjetas](../../assets/images/DER_Tarjetas.png)

_The following describes the use of the API methods of Accounts Cards and Cards of the OpenPass Platform in all its variants (Pre-pay, Credit, Debit, Giftcard)._
 
_The most relevant considerations for understanding and achieving better utilization of API methods are described._

### _Card Issuers_

_The card accounts are always associated with an Issuer which is previously created from the BackOffice of the OpenPass Platform._
 
_Once the Issuers with which the Platform will work have been created, they can be consulted using the API methods detailed in the "Issuers" section._
 
_This is necessary to then manage the Accounts-Cards and Cards of each individual._
 
### _Affinity groups_
 
_Likewise, the OpenPass Platform allows you to generate different Affinity Groups (affinities) for each issuer. In this way, you can achieve a logical grouping of Card Accounts within each Issuer based on different affinity groups._
 
_Just like Issuers, Affinity Groups are previously created from the OpenPass Platform BackOffice._
 
_Once the Affinity Groups that the Platform will work with have been created, they can be consulted using the API methods detailed in the "Affinity Groups" section._

### _Card Accounts_

_The OpenPass Platform handles the concept of Card Accounts which may or may not be associated with a Wallet Account. That is, a user could have on the OpenPass Platform one or more Financial Account (s) and one or more Card Accounts, but could also have only one Card Account and NO Financial Accounts._
 
_Within Card Accounts, you can differentiate the following types of cards:_

- _Prepaid._
- _Credit._
- _Debit._
- _GiftCard._

_The management of Card Accounts within the OpenPass Platform is done by the following methods, which are presented below grouped in a logical way only to make their interpretation easier._

### _Methods for Managing Accounts-Cards_

#### _Prepaid Card Holder Account Registration_
 
_The different types of Account Holder are registered using the following methods. These methods will generate an account of the corresponding type, associated with the User and automatically, the first time, will generate a Card of the corresponding type._

- _POST/cardHolderAccount/prepaid_
- _POST /cardHolderAccount/credit_
- _POST/cardHolderAccount/prepaid_
- _POST/cardHolderAccount/giftcard_

> _**NOTE**: While on the object being sent a issuerId is requested  in which it already is specified the type of account Holder to request (prepaid, credit, debit, giftcard), in the API calls it is also requested (by URL) so that, in the future, it is not mandatory to go through a discovery method of Issuers, and the platform can calculate it automatically._

#### _Card Account Consultation_
 
_This method is used to consult the existing Cardholder Accounts. This method returns an array of all active CardHolderAccount and CardHolderAccountRequest in pending state (see description of "pending state")._
 
_A CardHolderAccount is an object that combines: **Cardholder + Cardaccount + Card_account + [Cards]**._

- _GET/cardHolderAccounts/_

_Method to consult for a specific Card Holder Account._

- _GET/cardHolderAccount/{cardHolderAccountId}_

_Likewise, an Account Holder can be consulted via the clientTransactionId sent in the original Registration request if necessary._

- _GET/cardHolderAccount?clientTransactionId={value}_

#### _Changing Account Status of a Card Holder Account_

_Using the following methods you can change the status of a particular Cardholder account. Using these methods you can offer the user the administration and control of their Card Accounts._

- _POST/cardHolderAccount/{cardHolderAccountId}/action/activate_
- _POST/cardHolderAccount/{cardHolderAccountId}/action/deactivate_
- _POST/cardHolderAccount/{cardHolderAccountId}/action/disable_
- _POST/cardHolderAccount/{cardHolderAccountId}/action/freeze_
- _POST/cardHolderAccount/{cardHolderAccountId}/action/inhibit_

#### _Status Pending:_

_As with most transactions, the OpenPass Platform considers and manages all possible failures and/or interruptions in integrations with third parties (providers), in this case, card processors._
 
_These possible failures and/or interruptions can generate dis-synchronization between the different objects between the OpenPass Platform and the provider, for example. OpenPass may NOT receive a response to a card registration, so IT IS NOT created, but the provider creates it or vice versa._
 
_In this case, the OpenPass Platform will leave the CardHolderAccountRequest object as "pending" and then update its final status after synchronizing with the provider and confirming the actual/final status of the transaction through a pre-automated process._
 
_It is important to note that CardHolderAccountRequest in pending status can be confirmed and become CardHolderAccount or voided and removed from this query._


###  _<span style="text-decoration: underline">Card Handling Methods </span>_

#### _Card inquiry_

_The following method allows you to consult a specific card._
 
- _GET/cardHolderAccount/card/{cardId}_
 
_This method returns the complete data of the card, reconciling with an external provider._
 
_Consider that, while persisting objects are queried in the OpenPass Platform, the API can return errors related to the external search of sensitive card data._
 
_Sensitive data will be returned flat or encrypted as appropriate on different objects (CardData or CardEncryptedData)._

###  _<span style="text-decoration: underline">Regenerating a Card </span>_

_To perform the regeneration of a prepaid card you must use one of the following methods. You can regenerate both a card of the same type (virtual or physical) and of a different types (virtual to physical or vice versa)._
 
- _POST/cardHolderAccount/card/{cardId}/regeneration/regenerate_
 
_With the following Request, a new card is generated, both physical and virtual, leaving in a "DeactivatedRenovation" state the previous one of the same type if it existed._
 
- _POST/cardHolderAccount/{{cardHolderAccountId}}/card_

###  _<span style="text-decoration: underline">Change of Correspondence Address </span>_

_To modify the correspondence address of an Account Holder, to where the user wants the Physical Prepaid Card requested to be sent, the following method must be used._
 
_This information must be updated prior to requesting the Registration or Regeneration of a Physical Card._
 
- _PUT/cardHolderAccount/{cardHolderAccountID}/correspondence_

###  _<span style="text-decoration: underline">Affinity Group Modification </span>_

_To modify an Account's Affinity Group, use the following method:_

- _PUT/cardHolderAccount/cardHolderAccount/{cardHolderAccountID}_

_This information must be updated prior to requesting the registration or regeneration of a physical card in case the Affinity Group is being used as a logical grouping to, for example, have cards with different designs or different categories (e.g. Gold, Platimun, etc.)._

###  _<span style="text-decoration: underline">Enabling Physical Card </span>_

_After completing the Physical Card Request flow through the calls to the corresponding API methods according to each case, the process continues through the logistics circuit of Packaging and delivery of the plastic, which is independent of the OpenPass platform._
 
_When the user receives the card, he/she must enable it (for which the following method must be used)._
 
- _POST /cardHolderAccount/card/{cardId}/commercial-enable_
 
_This method is used to perform the commercial enablement of a card object. Returns the card object used. Automatically, when requesting the commercial authorization of a card, the existence and status of ALL existing cards will be synchronized for the Cardholder Account._
 
_Note that the card must be in "Packed" status to enable it. When the card is created, it is in a "PendingPacked" state. When the provider prints it, it automatically changes to a "Packed" state._

> _**NOTE**: Since some providers (in case the User previously has a Virtual card), at the time of Enabling the Physical Card, cancel the Virtual Card, in case you want to keep both cards active, after enabling the Physical one, you MUST change the status to the Virtual card, using the corresponding method, to activate it again._

- _POST /cardHolderAccount/card/{cardId}/action/activate_

###  _<span style="text-decoration: underline">Changing Account Status of a Card Holder Account </span>_

_Using the following methods you can change the status of a specific Cardholder account. These methods offer the user the administration and control of their Card Accounts._
 
_Modifiable statuses from the API_

- _Normal_
- _ReportTheft_
- _BlockedTemporal_
- _Deactivated_

#### _Use cases:_

- _"Normal": card in status to be used normally_

    - _POST / cardHolderAccount/card / {CardID} / action/ deactivate: changes to "Deactivated" status._

    - _POST / cardHolderAccount/card / {CardID} / action / lock-temporary, changes to "BlockedTemporal" state._

    - _POST / cardHolderAccount/card / {CardID} / action/ deactivate: changes to "Deactivated" status._

- _From "Normal" status to "ReportTheft". Report stolen._

    - _POST / cardHolderAccount/card / {CardID} / action / report-theft: cannot return to "Normal" state._

#### _Considerations:_

- > _The status "Deactivated" and "BlockedTemporal" have the same effect in Prepaid Cards. In both cases, consumption will be rejected if the card is in any of the aforementioned states._

- > _The "/activate " method is only valid for reverting the "Deactivated" and "BlockedTemporal" states. To "enable" a physical card you must use the method described above._

####  _<span style="text-decoration: underline">Statuses not modifiable from the API reported by the provider </span>_

- _DeactivatedRenovation_
- _Pending_
- _Overdue_
- _Packed_
- _PendingPacked_
- _NotEnabledIvr_
- _RenovationlockedIvr_
- _DenouncedIvr_
- _Unknown_
