# Servicios Bancarios

## Introducción

La Plataforma OpenPass se encuentra integrada con diferentes proveedores de Servicios Bancarios (mejor conocidos como API Bank) mediante los cuales brinda a los usuarios de la Plataforma conexión con el Sistema Financiero/Bancario.

Con los métodos detallados dentro del grupo Bank Services se podrán gestionar los diferentes servicios disponibles.

Para poder hacer uso de estos servicios, el Cliente deberá estar registrado como PSP (Proveedor de Servicios de Pago) antes el BCRA (Banco Central de la República Argentina) lo cual lo habilita a gestionar Cuentas Virtuales (CVUs) y utilizar las mismas para transaccionar contra el Sistema Financiero y/o contra otras Fintech que gestiones CVUs.

## Detalle de métodos de Servicios Bancarios

La plataforma OP cuenta con varios métodos relacionados al CVU de una cuenta.

* POST - /bank-services/cvu.

* GET - /bank-services/cvu.

* PUT - /bank-services/cvu/alias.

* POST - /transfer/outer/bank-transfer/beneficiary-discovery.

* GET - /transfers/outer/bank-transfers/cvu-transfers.

* POST - /transfer/outer/bank-transfer/cvu-transfer/.

* GET - ​​/transfer/outer/bank-transfer/cvu-transfer/{id}.

#### **_POST - /bank-services/cvu_**

Mediante este método se da de Alta el CVU asociado a una cuenta. Cabe aclarar que es requisito que la cuenta **EXISTA** en la Plataforma OpenPass y que el usuario ya cuente con el CUIT registrado en la plataforma.
 
El proceso creará el CVU e intentará crear el ALIAS como un proceso orquestado dentro de la Plataforma OpenPass (siempre con el concepto de Best Effort) y basado en el patrón definido (ver "**ANEXO I - Configuración de Patrón de Creación de ALIAS**").
 
Se pueden dar 3 situaciones diferentes:

* **_Se crea el CVU y ALIAS_**: El método devolverá un **OK**, el valor del CVU y el ALIAS.

* **_Se crea el CVU y ALIAS NO_**: Si por algún motivo se encontrara un error en el proceso de creación del Alias (de comunicación, falta de respuesta del API Bank o cualquier otro tipo de error que imposibilite la creación del Alias) el mismo **NO** será devuelto en el response del método (el campo “_{label}_” **NO** será devuelto).

**IMPORTANTE**: En este caso, como el CVU **SI** pudo ser creado, el método devolverá un OK y el valor del CVU. Es decir, el hecho de poder o no generar el ALIAS, **NO** interfiere con la creación del CVU.

* **_No se crea el CVU ni el ALIAS_**: El método devolverá ERROR.

Independientemente que es “recomendable” que el método se utilice solo para dar de Alta CVUs, el proceso de todas maneras valida, previamente a la creación del CVU, que la cuenta NO tenga uno previamente creado y asociado en la base de datos de la Plataforma OpenPass pudiendo darse 3 situaciones diferentes:

* _CVU y ALIAS NO existen_: El proceso funcionará según lo descrito anteriormente.
 
* _CVU existe y ALIAS NO existe_: Por una cuestión de mantener constantemente una sincronización entre los diferentes repositorios de datos, el proceso en este caso funcionará igual que el proceso anterior validando con el API Bank correspondiente la creación del CVU y volverá a intentar dar de alta el ALIAS con las mismas consideraciones antes descritas.
 
* _CVU y ALIAS existen_: En este caso, el proceso devuelve la información que tiene almacenada en las bases de datos de la Plataforma OP.

> Es altamente recomendado **NO** utilizar este método como “consulta” del CVU asociado a una cuenta. En ese caso se debe/recomienda utilizar el método correspondiente.

#### **_GET - /bank-services/cvu_**

Mediante este método se consulta el CVU y ALIAS asociado a una cuenta, siempre y cuando este haya sido creado previamente. La información se toma de las bases de datos de la Plataforma OpenPass.
 
Como parte del proceso, se pueden dar diferentes situaciones en las cuales el método trabajará según se detalla a continuación:

* **_CVU y ALIAS existentes_**: El método devolverá ambos valores en su "response".

* **_CVU existe y ALIAS NO existe_**: El proceso intentará crear el ALIAS como un proceso orquestado dentro de la Plataforma OpenPass (siempre con el concepto de Best Effort) y basado en el patrón definido (ver "**ANEXO I - Configuración de Patrón de Creación de ALIAS**").

Si por algún motivo se encontrara un error en el proceso de creación del Alias (de comunicación, falta de respuesta del API Bank o cualquier otro tipo de error que imposibilite la creación del Alias) el mismo **NO** será devuelto en el response del método (el campo “_{label}_” **NO** será devuelto)

* **_CVU y ALIAS NO existen_**: El método devolverá “vacío” en su "response" y **NO** devolverá el atributo “_{label}_”.

> **_NOTA_**: En todos los casos donde los procesos intenten crear un ALIAS asociado al CVU de una cuenta y este **NO** pueda ser generado, la Plataforma OP el campo “_{label}_” **NO** será devuelto, caso contrario se devolverá el valor del ALIAS generado.

#### **_PUT - /bank-services/cvu/alias_**

Mediante este método se da de alta el ALIAS asociado a un CVU de una cuenta. Cabe aclarar que es requisito que la cuenta y el CVU **EXISTAN** en la Plataforma OpenPass.
 
Independientemente de que la cuenta tenga un ALIAS previamente generado o no, el proceso modificará el valor del ALIAS asociado a dicha cuenta tanto en sus bases de datos como en el API Bank correspondiente.

#### **_POST - /transfer/outer/bank-transfer/beneficiary-discovery_**

Mediante este método se obtienen, a través del ALIAS o CVU de una cuenta, los datos relativos al CUIT relacionado a ésta junto al nombre y apellido del titular y la denominación de la cuenta (Caja de Ahorro).

#### **_GET - /transfers/outer/bank-transfers/cvu-transfers_**

Mediante este método se pueden consultar las transferencias salientes realizadas hacia otros CVUs/CBUs .

#### **_POST - /transfer/outer/bank-transfer/cvu-transfer/_**

Mediante este método se realizan transferencias hacia otros CVUs/CBUs desde la Cuenta Virtual del usuario (CVU). 

#### **_GET - ​​/transfer/outer/bank-transfer/cvu-transfer/{id}_**

Mediante este método se pueden consultar una transferencias salientes en particular mediante el {id} y obtener todos los datos de la misma. 

El {id} se puede obtener de la lista devuelta por el método **_GET  - /transfers/outer/bank-transfers/cvu-transfers_**.

## ANEXO I - Configuración de Patrón de Creación de ALIAS

<span style="text-decoration:underline;">**_Patrón definido para la creación del ALIAS_**</span>:

A continuación, se detalla el patrón definido de manera conjunta entre OP y FT para la creación del alias.
 
El mismo está basado en un esquema de reemplazo de variables y es configurado desde el backoffice de la plataforma y puede ser cambiado de acuerdo con la necesidad que se presente desde la opción administración de conectores para el correspondiente al BIND, tal como se muestra en la siguiente figura:

![](../../assets/images/rui_alias.png)

Está conformado por un “aliasTemplate” y un “aliasDomain” opcional
 
El patrón del “aliasTemplate” se podrá formar mediante la combinación de 4 variables opcionales que en su totalidad no deberán sumar los 20 caracteres.
 
Las variables disponibles para el patrón son:

    %1 - Primera letra nombre 

    %2 - Primer Nombre Completo 

    %3 - Primer letra apellido 

    %4 - Apellido completo

Las 4 variables obtienen su valor de los datos de la Billetera. En caso de duplicidad, se agregara un número incremental en la última posición antes del aliasDomain (si estuviera definido)
 
A modo de ejemplo les incluyo como sería el patrón (en amarillo) y su aplicación en base a una billetera cuyo titular tiene como nombre **Juan**, apellidos **De los Palotes** y aliasDomain= **bhub**.  Lo primero que haremos antes de aplicar el patrón es eliminar todos los espacios del nombre y apellido y luego reemplazamos todo carácter no admisible en el estándar de CVU/CBU, como la letra ñ, por citar un ejemplo.


1. '%1%4.$aliasDomain' => jdelospalotes.bhub.

2.  '%2.%4.$aliasDomain' => juan.delospalot.bhub (Se auto-recortó el largo, hasta llegar a 20 caracteres).

3. ‘%2.%4.$aliasDomain” => juan.delospalo1.bhub (Alias incrementado por duplicidad).

<span style="text-decoration:underline;">**_Posible Caso de Uso_**</span>:

Si bien pueden existir varios flujos y/o casos de uso según se defina durante la etapa de creación del Producto, aquí detallamos unos de ellos (el más común) a modo de ejemplo.
 
En este caso se describe un posible “**_proceso orquestado_**” del lado de quien consume los servicios de OP con la intención de dar de Alta la CUENTA, el CVU y el ALIAS dentro del mismo proceso.

* **ALTA DE CUENTA:** _POST - /account/finalConsumerAccount/register_ para dar de alta una nueva cuenta.

* **Alta de CVU:** POST - /bank-services/cvu para dar de alta CVU y ALIAS.

En este caso, pueden darse las siguientes 3 situaciones diferentes:

1. Exitosa y completa: Cuenta + CVU + Alias
2. Exitosa y parcial (1): Cuenta + CVU (~~Alias~~)
3. Exitosa y parcial (2): Cuenta (~~CVU~~ - ~~Alias~~)

> **NOTA:** En **TODOS** los casos, si el método “_Register_” termina **OK**, la cuenta **SERÁ** creada en la Plataforma OP. Más allá de que quien consuma los servicios lo trabaje de manera orquestada, para la Plataforma OP los métodos “_Registrer_” y “_CVU_” con procesos completamente independientes.

**1. Exitosa y completa: Cuenta + CVU + Alias**

En este caso no es necesario tomar ninguna acción desde lo funcional y/o flujo.

![](../../assets/images/proceso_register_cvu_exitoso.png)

**2. Exitosa y parcial (1): Cuenta + CVU + (~~Alias~~)**

En este caso no es necesario tomar ninguna acción desde lo funcional y/o flujo al momento del Alta de Cuenta dado que el Cliente puede operar con su Billetera ya que para eso solo es necesario contar con CVU.
 
No tendría sentido penalizar el Alta de la Cuenta por no haber podido generar el Alias.
 
De manera asincrónica, es decir, en cualquier otro momento, se puede volver a intentar generar el Alias mediante el método _GET - /bank-services/cvu_ tal como se explicó previamente.
 
Por ej: Dentro de una sección de Perfil de Usuario o Datos Financieros, al momento de mostrar el CVU, se debe llamar a dicho método y el mismo proceso intentará volver a crear el Alias en caso de no encontrarlo en la base de la Plataforma OP.

![](../../assets/images/proceso_register_cvu.png)

> **NOTA:** SI bien el ALIAS **NO** pudo ser generado en el momento del Alta de Cuenta y CVU, **NO** es necesario ni recomendable volver a realizar un POST de CVU, sino que se DEBE realizar un GET de CVU y nunca volver a llamar al “_Register_” ya que la cuenta **EXISTE** en la Plataforma OP.

**3. Exitosa y parcial (2): Cuenta (~~CVU~~ - ~~Alias~~)**

En este caso se debe tener en cuenta que el Cliente **NO** puede operar con su Billetera ya que en este caso falló la creación del CVU, pero **SÍ** es importante tener en cuenta que la Cuenta **SI** fue creada en la Plataforma OP.

> **NOTA**: Si bien el CVU y ALIAS **NO** pudieron ser generados en el momento del Alta de Cuenta, **NO** se debe volver a llamar al “_Register_” ya que la cuenta **EXISTE** en la Plataforma OP.

En este punto se deberá evaluar desde Producto y/o UX/UI como se generará el CVU y ALIAS en una instancia posterior y/o desde que opción de menú y/o funcionalidad.
 
Llegado dicho momento, se debe llamar al método _POST - /bank-services/cvu_ el cual intentará generar el CVU y el Alias según se describió anteriormente.

![](../../assets/images/proceso_register_cvu_fallido.png)



# _Bank Services_

## _Introduction_

_The OpenPass Platform is integrated with different Banking Services providers (better known as API Bank) through which it provides users connection to the Financial/Banking System._

_The detailed methods within the Bank Services group allow for the management of the different services available._

_In order to use these services, the Client must be registered as a PSP (Payment Service Provider) at the BCRA (Central Bank of the Argentine Republic) which enables the management of Virtual Accounts (CVUs) and their usage to transact with the Financial System and/or with another Fintech that manage cvus._

## _Detail of Banking Services methods:_

_The OP platform has several methods related to the CVU of an account._

-	_POST - /bank-services/cvu_

- _GET - /bank-services/cvu_

-	_PUT - /bank-services/cvu/alias_

-	_POST - /transfer/outer/bank-transfer/beneficiary-discovery_

-	_GET - /transfers/outer/bank-transfers/cvu-transfers_

-	_POST - /transfer/outer/bank-transfer/cvu-transfer/{id}_

-	_GET - /transfer/outer/bank-transfer/cvu-transfer/{id}_

#### _POST - /bank-services/cvu_

_This method is used to register the CVU associated to an account. It should be clarified that it is a requirement that the account **EXISTS** in the OpenPass Platform and that the user already has the CUIT registered in the platform._

_The process will create the CVU and try to create the ALIAS as an orchestrated process within the OpenPass Platform (always with the Best Effort concept) and based on the defined pattern (see "**ANNEX I - ALIAS Creation Pattern Configuration**")._

_There can be 3 different situations:_

-	_**The CVU and ACCOUNT ALIAS are created**: the method will return an **OK**, the CVU value and the ALIAS._

-	_**The CVU is created, but the ACCOUNT ALIAS NO is not**: if for any reason there is an error in the process of creating the Alias (communication, lack of response of the API Bank or any other type of error that prevents the creation of the Alias) it will **NOT** be returned in the response of the method (the “{label}"**WILL NOT*** be returned)._

_**IMPORTANT**: In this case, as the CVU could be created, the method will return an OK and the value of the CVU. That is, the fact of being able or not to generate the ALIAS, DOES NOT interfere with the creation of the CVU._

-	_**No CVU or ALIAS is created**: The method will return ERROR._

_Regardless it being "recommended" that the method is used only to register CVUs, the process validates prior to the creation of the CVU that the account DOES NOT have one previously created and associated in the database of the OpenPass Platform being able to give 3 different situations:_

-	_**CVU and ALIASES DO NOT exist**: The process will work as described above._

-	_**CVU exists and ALIAS DOES NOT exist**: as the aim is to constantly maintain a synchronization between the different data repositories, the process in this case will work the same as the previous process validating with the corresponding API Bank the creation of the CVU and will again try to register the ALIAS with the same considerations described above._

-	_**CVU and ACCOUNT ALIAS exist**: In this case, the process returns the information it has stored in the databases of the OP Platform._

> _It is highly recommended **NOT** to use this method as a “query” of the CVU associated with an account. In this case the appropriate method should be used._

#### _GET - /bank-services/cvu_

_This method queries the CVU and Account Alias if they were previously created. The information is taken from the databases of the OpenPass Platform._

_As part of the process, there may be different situations in which the method will work as detailed below:_

-	_**Existing CVU and ACCOUNT ALIAS**: The method will return both values in its "response"._

-	_**CVU exists, ACCOUNT ALIAS doesn't**: The process will create the CVU and try to create the ACCOUNT ALIAS as an orchestrated process within the OpenPass Platform (always with the Best Effort concept) and based on the defined pattern (see "**ANNEX I - ALIAS Creation Pattern Configuration**")._

_If for any reason there is an error in the process of creating the Alias (communication, lack of response of the API Bank, or any other type of error that prevents the creation of the Alias) it will **NOT** be returned in the response of the method (the “{label}" WILL **NOT** be returned)._

-	_**CVU and ACCOUNT ALIAS DO NOT exist**: The method will return "empty" in its "response" and WILL **NOT** return the attribute “{label}”._

> _**NOTE**: In all cases where the processes try to create an ACCOUNT ALIAS associated with the UCV of an account and it cannot be generated, the Platform OP the field “{label}" WILL **NOT** be returned. O.therwise the value of the generated ALIAS will be returned._

#### _PUT - /bank-services/cvu/alias_

_This method is used to register an account alias associated with an account UCV. It should be clarified that it is a requirement for the account and the CVU to EXIST in the OpenPass Platform._

_Regardless of whether the account has a previously generated ACCOUNT ALIAS or not, the process will modify the value of the ALIAS associated with that account, both in its databases and in the corresponding API Bank._

#### _POST - /transfer/outer/bank-transfer/beneficiary-discovery_

_This method is used to get the data of the CUIT related to an account along with the name and surname of the holder and the denomination of the account (Savings Bank Account)._

#### _GET - /transfers/outer/bank-transfers/cvu-transfers_

_This method is used to consult transfers made to other CBU/CVU._

#### _POST - /transfer/outer/bank-transfer/cvu-transfer/{id}_

_Through this method, transfers are made to other CVUs/CBUs from the user's Virtual Account (UCV)._

#### _GET - /transfer/outer/bank-transfer/cvu-transfer/{id}_

_This method is used to consult a specific outgoing transfer using the {id} and get all its data._

_The {id} can be obtained from the returned list by the method GET - /transfers / outer / bank-transfers / cvu-transfers._

## _ANNEX I - ACCOUNT ALIAS Creation Pattern Configuration_

<span style="text-decoration:underline;">**_Pattern defined for ALIAS creation:_**</span>:

_The pattern defined jointly between OP and FT for creating the alias is detailed below._

_It is based on a variable replacement scheme, it is set up at the backoffice of the platform and can be changed according to the arising needs through the connector administration option corresponding to the BIND, as shown in the following figure:_

![](../../assets/images/rui_alias.png)

_It consists of an "aliasTemplate" and an optional” aliasDomain"_ 

_The pattern of the "aliasTemplate" can be formed by combining 4 optional variables that in their entirety should not add the 20 characters._

_The variables available for the pattern are:_

    %1 - First letter name
    %2 - First Full Name
    %3 - First letter last name
    %4 - Full name

_The 4 variables get their values from the Wallet data. In case of duplication, an incremental number will be added in the last position before the aliasDomain (if defined)._

_As an example, I include them as would be the pattern (in yellow) and its application based on a wallet whose holder's name is **Juan**, surnames **De los Palotes** and aliasDomain = **bhub**. The first thing we will do before applying the pattern is to remove all spaces from the first name and last name, and then replace any non-admissible characters in the CVU/CBU standard, such as the letter "ñ"._

1. 	_'%1%4.$ aliasDomain ' = > jdelospalotes.bhub._
2.	_'%2.%4.$ aliasDomain' = > juan.delospalot.bhub (Self-trimmed the length to 20 characters)._
3.	_‘%2.%4.$ aliasDomain” = > john.delospalo1.bhub (Alias increased by duplicity)._

<span style="text-decoration:underline;">**_Possible Use Case:_**</span>:

_Although there may be several flows and/or use cases as defined during the Product creation stage, here we detail some of them (the most common) as an example._

_In this case, we describe a possible “**orchestrated process**” on the side of those who consume OP services with the intention of registering the ACCOUNT, the CVU and the ALIAS within the same process._

-	_**ACCOUNT REGISTRATION**: POST - /account/finalConsumerAccount /register to register a new account._

-	_**CVU registration**: POST - /bank-services/cvu to register CVU and ALIAS._

_In this case, the following 3 different situations may arise:_

1.	_Successful and complete: Account + CVU + Alias_
2.	_Successful and partial (1): Account + CVU (~~Alias~~)_
3.	_Successful and partial (2): Account (~~CVU~~ - ~~Alias~~)_

> _**NOTE**: In **ALL** cases, if the method “Register" ends as **OK**, the account **WILL** be created on the OP Platform. Beyond the fact that those who consume the services work in an orchestrated way, for the OP Platform the methods “Register” and CVU" are completely independent processes._

1. _**Successful and complete: Account + CVU + Alias**_

_In this case, it is not necessary to take any action from the functional and/or flow._

![](../../assets/images/proceso_register_cvu_exitoso.png)

2. _**Successful and partial (1): Account + CVU (~~Alias~~)**_

_In this case, it is not necessary to take any action from the functional and/or flow at the time of Account Registration since the Client can operate with its Wallet since it is only necessary to have CVU._

_It would not make sense to punish the Account for not having been able to generate the Alias._

_Asynchronously, i.e. at any other time, you can try to generate the Alias again using the method GET-/bank-services/cvu as explained above._

_For example: within a User Profile or Financial Data section, when displaying the CVU, this method must be called for and the process will try to recreate the Alias if it is not found in the base of the OP Platform._

![](../../assets/images/proceso_register_cvu.png)

> _**NOTE**: altthough the ALIAS could **NOT** be generated at the time of Account and CVU Registration, it is **NOT** necessary or advisable to perform a CVU POST again, but a CVU GET must be performed and never call the “Register" since the account **EXISTS** in the OP Platform._

3. _**Successful and partial (2): Account (~~CVU~~ - ~~Alias~~)**_

_In this case, it should be taken into account that the Client can NOT operate with his Wallet since the creation of the CVU failed, but it is important to note that the IF an account was created on the OP Platform._

> _**NOTE**: Although the CVU and ALIAS could **NOT** be generated at the time of Account Registration, you should **NOT** call the “Register" since the account **EXISTS** on the OP Platform._

_At this point you should evaluate from the Product and/or UX/UI Team how the CVU and ALIAS will be generated in a later instance and/or from which menu option and/or functionality._

_The POST - /bank-services/cvu method must be called, and it will attempt to generate the CVU and Alias as described above._

![](../../assets/images/proceso_register_cvu_fallido.png)

