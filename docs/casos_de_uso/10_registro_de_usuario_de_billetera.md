# Registro de Usuario de Billetera

Mediante el método “_Register_” se dan de alta cuentas en la Plataforma OP.
 - **POST** - /account/finalConsumerAccount/register

El registro de cuentas se complementa con todas sus alternativas de agregado y modificación de datos mediante una serie de métodos asociados en caso de que se quiera realizar una carga de información del usuario de manera progresiva, es decir, realizar un alta de cuenta inicial con los datos mínimos y luego, según el cliente vaya necesitando/queriendo, irá agregando el resto de sus datos nominativos.

*Por Ejemplo: email, phone, tributary-info, etc.*

El método Register es el principal de la Plataforma OpenPass ya que el resto de los métodos y funciones necesitan contar con una cuenta creada en el Sistema.

> **NOTA**: Siempre que el método “_Register_” devuelva un **OK**, significa que la cuenta **YA** fue creada en la Plataforma OP, con lo cual, en caso de volver a llamar al método con los mismos parámetros, el proceso devolverá un error por “_Cuenta Existente”_

Si bien la Plataforma OP permite manejar un ID de Cliente Externo (El ID mediante el cual quien consume los servicios de OP identifica sus cuentas), se recomienda de todas maneras persistir el ID de Cuenta de OP con el objetivo de mantener una mejor sincronización de datos entre plataformas, permitir un mejor troubleshooting, etc.

![](../../assets/images/proceso_register.png)
