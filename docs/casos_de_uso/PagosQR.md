# Pagos QR (3.0) - Overview

## Introducción

El presente documento describe el uso de las APIs para poder realizar Transferencias (mediante el modelo de Transferencia 3.0) utilizando la lectura de QRs Interoperables.

Las APIs mencionadas se basan en el standard de QR Interoperable EVMCo LLc 

Con los métodos detallados dentro del grupo Pagos QR se podrán gestionar los diferentes servicios disponibles.

## Detalle de métodos de Pagos QR

La plataforma OP cuenta con varios métodos relacionados al CVU de una cuenta.

* POST - /payments/qr-payment

* GET - /payments

* GET - /payments/{id}

El manejo de los estados de este tipo de Transacciones es igual al del resto de las transacciones dentro de BHUB. Las mismas pueden quedar en estado Approved, Rejected o Pending. En este último caso, el estado final podrá ser consultado mediante los métodos detallados más adelante o también podrán ser informados los cambios mediante los mecanismos de webhooks.

#### **_POST - /payments/qr-payment_**

Mediante este método se generan las Transferencias hacia un destinatario utilizando los datos obtenidos de un QR Interoperable.

El método cuenta con la inteligencia necesaria para poder ser utilizado de 2 maneras diferentes:
* Pidiendo que el parseo del QR lo realice BHUB: No se requiere un parseo previo, se envía el código QR en formato de cadena de caracteres y BHUB se encarga de eso. (en este caso deberá enviarse como className **QrCodeData**)

* Enviando los datos normalizados: Se requiere del parseo del QR leído, previo al llamado al método. De esta forma se envía la información en atributos diferenciados tales como:  los datos del Destination en los atributos del objeto **PaymentData** (Por ej.: _TributaryInfo_, _BankRouting_, etc). De esta forma BHUB no se encargará del parseo del QR y tomará los datos que se indiquen en cada campo, utilizará lo que se haya enviado allí al realizar la transferencia mediante el proveedor que corresponda. (en este caso deberá enviarse como className **QrPaymentData**)

En ambos casos se debe enviar el String obtenido del QR (atributo: _code_).


> **NOTA:** En el atributo _codeFormat_ se debe enviar el Formato de QR con el cual se está trabajando. Por el momento solo se trabaja con el Standard EMVCo, pero a futuro podría existir múltiples formatos de QR diferente.


<span style="text-decoration:underline;">**_Consideraciones_**</span>:

Es importante tener en cuenta lo siguiente al momento de realizar el llamado a este método.

Debido a que los QR pueden ser tanto Estáticos como Dinámicos y contener o no el dato de Importe dentro del mismo el comportamiento del BHUB se basa en las siguientes premisas:

* Si el parseo del QR lo realiza BHUB y encuentra un dato de Importe dentro del String del QR (Dinámico), tomará éste como el Importe de la transferencia, caso contrario tomará el importe informado en el atributo _amount_ de la transacción.

* Si el parseo del QR **NO** lo realiza BHUB, el Importe de la transferencia será tomado del atributo _amount_ de la transacción.

#### **_GET - /payments_**

Mediante este método se consultan todos los payments realizados por una cuenta.

#### **_GET - /payments/{id}_**

Mediante este método se consulta un payment en particular mediante el {id} correspondiente.

> Se adjunta un ejemplo para probar el POST de qr-payments y se detallan los casos a probar:
>
> - Transacción realizada con éxito: Se debe ingresar un "amount" menor a 3000 e impar.
> - Error de aplicación: Se debe ingresar un "amount" menor a 3000 y par.
> - Transacción pendiente de confirmación: Se debe ingresar un "amount" mayor o igual a 3000 y menor a 4000.
> - Delay: Se debe ingresar un "amount"  mayor a 3000 y menor a 4000, el tiempo de Delay  es el valor que se ingrese menos 3000 (Es decir, si se ingresa 3100 se generará un delay de 100 segundos).
> - Errores: Se debe ingresar un "amount"  mayor o igual a 4000. Devuelve un error con código igual al valor que se ingresó menos 4000, es decir: codigo error = (valor ingresado - 4000). Por ejemplo para el código de error 344 se obtiene un "Cantidad scoring diario fuera rango", para que devuelva esto debería ingresarse un "amount" de 4344.
> - Simulación de Webhook de confirmación: Se debe ingresar un "amount" mayor o igual a 300 y menor a 4000 terminando el mismo con 0, 1, 2 o 3. De esta forma se simulará una confirmación de la transacción tal como lo hace Coelsa con el método QROperacionFinalizada, dejando la transacción en estado "Confirmada".
> - Simulación de Webjook de rechazo: Se debe ingresar un "amount" mayor o igual a 300 y menor a 4000 terminando el mismo con 4, 5, 6 o 7. De esta forma se simulará el rechazo de la transacción tal como lo hace Coelsa con el método QROperacionFinalizada, dejando la transacción en estado "Anulada".






# _QR Payments (3.0) - Overview_

## _Introduction_

_This document describes the use of APIs to perform Transfers (by menas of the Transfer model 3.0) using Interoperable QRs reading.
The APIs mentioned are based on the EVMCo LLc Interoperable QR standard._

_The detailed methods within the QR Code Payments group allow for the management of the different services available._

## _Detail of Banking Services methods:_

_The OP platform has several methods related to the CVU of an account._

-	_POST - /payments/qr-payment_
-	_GET - /payments_
-	_GET - /payments/{id}_

_The handling of the status of this type of Transaction is the same as the other transactions within BHUB. The status can be Approved, Rejected or Pending. In the latter case, the final status can be consulted through the methods detailed below or changes can also be informed through the mechanisms of webhooks._

#### _POST - /payments/qr-payment_

_This method generates transfers to a recipient using data obtained from an Interoperable QR._

_The method has the necessary intelligence to be used in 2 different ways:_

-	_Asking for the QR parsing to be done by BHUB: no prior parsing is required, the QR code is sent in string format and BHUB takes care of that. (in this case it must be sent as className **QrCodeData**)._

-	_Sending the normalized data: it is required to parse the QR read, prior to calling the method. The information is sent in differentiated attributes such as the Destination data in the attributes of the **PaymentDat**a object (e.g.: Tax Info, BankRouting, etc). Thus, BHUB will not be responsible for the QR parsing and will take the data indicated in each field, and use what has been sent when making the transfer through the corresponding provider. (in this case it must be sent as className **QrCodeData**)._

_In both cases you must send the String obtained from the QR (attribute: code)._

> _**NOTE**: In the codeFormat attribute, you must send the QR Format with which you are working. At the moment we only work with the EMVCo Standard, but in the future there could be multiple different QR formats._

<span style="text-decoration:underline;">**_Considerations_**</span>:

_It is important to keep in mind the following when calling this method._

_Because QR can be both Static and Dynamic and contain or not the Amount data within it, BHUB behavior is based on the following premises:_

-	_If the QR parsing is done by BHUB and finds an Amount data inside the QR String (Dynamic), it will take it as the Amount of the transfer, or it will take the amount reported in the attribute amount of the transaction._
-	_If the QR parsing is **NOT** performed by BHUB, the Transfer Amount will be taken from the attribute amount of the transaction._

#### _GET - /payments_

_This method consults all payments made by an account._

#### _GET - /payments/{id}_

_Method to consult a specific payment using the corresponding {id}._

> 	_An example to test the qr-payments POST is attached and the cases to be tested are detailed:_
> - _Successful transaction: an "amount" less than 3000 and odd number must be entered._
> -	_Application error: an "amount" less than 3000 and even number must be entered._
> -	_Transaction pending confirmation: an "amount" greater than or equal to 3000 and less than 4000 must be entered._
> -	_Delay: you must enter an "amount" greater than 3000 and less than 4000, the delay time is the value that is entered minus 3000 -that is, if you enter 3100 a delay of 100 seconds will be generated._
> -	_Errors: an "amount" greater than or equal to 4000 must be entered. Returns an error with code equal to the value entered minus 4000, i.e.: error code = (value entered - 4000). For example for the error code 344 you get a "Daily scoring amount out of range", to return this you should enter an "amount" of 4344._
> -	_Webhook Confirmation Simulation: an "amount" greater than or equal to 300 and less than 4000 and ending in 0, 1, 2, or 3 must be entered. This way, a confirmation of the transaction will be simulated as Coelsa does with the Qroperacionfinalized method, leaving the transaction as "Confirmed"._
> -	_Webhook Confirmation Simulation: an "amount" greater than or equal to 300 and less than 4000 and ending in 4, 5, 6, or 7 must be entered. This way, a confirmation of the transaction will be simulated as Coelsa does with the Qroperacionfinalized method, leaving the transaction as "Confirmed"._