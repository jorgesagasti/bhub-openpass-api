# Cuentas de Ahorro

#### Objetivo
<p style="text-align: justify;">
El objetivo de este manual es abordar de una manera simple la implementación de nuestras APIs.
</p>
<p style="text-align: justify;">
En el recorrido de los distintos ítems que trabajemos, se podrán comprender la integración de las distintas soluciones ofrecidas por la Plataforma OpenPass, para que puedan ser incorporadas a sus productos y servicios ofrecidos.
</p>

#### Introducción

<p style="text-align: justify;">
El presente documento describe el uso de las APIs de Cuentas de Ahorro (Saving Accounts) dentro de la familia de Cuentas Financieras (Financial Accounts) de la Plataforma OpenPass. 
</p>
<p style="text-align: justify;">
Las Cuentas de Ahorro son subcuentas de Financial Account que se desprenden de la Cuenta Billetera de cada usuario las cuales tienen características particulares. 
</p>
<p style="text-align: justify;">
En las secciones siguientes se detallan los métodos mediante los cuales se pueden gestionar este tipo de cuentas y se describen algunos casos de uso en cada uno de ellos.
</p>
<p style="text-align: justify;">
Debido a que la Plataforma OpenPass se encuentra en constante evolución, ya sea por mejoras incorporadas, nuevas funcionalidades, cambios normativos y/o cambios en los proveedores y con el afán de lograr una documentación actualizada, los detalles técnicos de cada uno de los métodos y sus atributos se encuentran publicados en formato OpenAPI en este mismo sitio. Por ese mismo motivo, no generamos documentos estáticos.
</p>

> Podrán observarse los métodos utilizados por la plataforma para las Cuentas de Ahorro buscando los endpoints correspondientes a **“/financial-account/”** y **"/financial-account/saving-account/"**



-------


###### Financial Account

<list>
<li style="margin-left: 40px;">
POST /financial-account/<b>{financial-accountId}</b>/add-funds
</li>
<li style="margin-left: 40px;">
GET /financial-account/<b>{id}</b>
</li>
<li style="margin-left: 40px;">
GET /financial-account/<b>{id}</b>/balance-detail
</li>
<li style="margin-left: 40px;">
GET /financial-account/<b>{id}</b>/movements
</li>
<li style="margin-left: 40px;">
GET /financial-account/<b>{id}</b>/movements/<b>{movementId}</b>
</li>
<li style="margin-left: 40px;">
GET /financial-accounts/
</li>
<li style="margin-left: 40px;">
GET /financial-accounts/concertation-accounts
</li>
</list>
<br>

###### Financial Available Account
<list>
<li style="margin-left: 40px;">
GET /financial-accounts/available-accounts
</li>
</list>
<br>

###### Financial Saving Account

<list>
<li style="margin-left: 40px;">
GET /financial-accounts/saving-accounts
</li>
<li style="margin-left: 40px;">
POST /financial-account/saving-account
</li>
<li style="margin-left: 40px;">
POST /financial-account/saving-account/<b>{financialAccountId}</b>/close
</li>
<li style="margin-left: 40px;">
PUT /financial-account/saving-account/<b>{financialAccountId}</b>
</li>
</list>
<br>

###### Financial Prepaid Account

<list>
<li style="margin-left: 40px;">
POST /financial-account/prepaid-account/<b>{prepaidAccountId}</b>/add-funds
</li>
<li style="margin-left: 40px;">
GET /financial-account/prepaid-accounts
</li>
</list>
<br>

------

#### Resumen
<br>

###### Consultar todas las Cuentas Financieras.

- **GET /financial-accounts**
<p style="text-align: justify;">
El endpoint devolverá todas las cuentas financieras en forma de array con un objeto por cuenta, asociadas a una billetera. Devuelve tanto las disponibles (available accounts) como las cuentas ahorro (saving accounts).
</p>

> **NOTA:** Available account y Saving account son dos tipos de cuentas distintas. No confundir Available con “disponibilidad”

**Response:**
*En la respuesta se observa una cuenta de cada tipo.*
```json
[
    {
        "className": "FinancialAvailableAccount",
        "id": "MvTxNsOS91P0oAm8Slvpj0470xmjMUh0MlSvUEya4nYbHe3PNFSOsp68lRvviEQEiTDIQuKnKRBTbq5_ohsXvg",
        "name": "Disponible",
        "description": "Pesos Argentinos",
        "isDefault": true,
        "balanceAmount": 1304.86,
        "arrangedAmount": 0.00,
        "overdraftCredit": 0.00,
        "availableCredit": 0.00,
        "enable": true,
        "product": {
            "code": "$",
            "id": "05C940B841BB20A070FC",
            "name": "Pesos Argentinos"
        }
    },
    {
      "className": "FinancialSavingAccount",
      "id":"MvTxNsOS91P0oAm8Slvpj0470xmjMUh0MlSvUEya4nYbHe3PNFSOsn1mC85Koo8aIZh5P_PtSRX7RlEdLqd7Rw",
      "name": "Mi cuenta description",
      "description": "Pesos Argentinos",
      "isDefault": false,
      "balanceAmount": 4.14,
      "arrangedAmount": 0.00,
      "overdraftCredit": 0.00,
      "availableCredit": 0.00,
      "enable": true,
      "product": {
          "code": "$",
          "id": "05C940B841BB20A070FC",
          "name": "Pesos Argentinos"
      },
      "creationDate": "2021-01-20T18:31:25-03:00"
    }
]
```

--------

###### Creación de Cuentas de Ahorro
 

*Las Cuentas de Ahorro se generan mediante el método: *

- **POST /financial-account/saving-account**
<p style="text-align: justify;">
Se pueden crear tantas cuentas de ahorro como sea necesario, cada uno podrá ser identificada por su ID, el cual es unívoco para cada cuenta de ahorro.
</p>
<p style="text-align: justify;">
Para una mejor identificación de la cuenta por el usuario, cada cuenta tendrá su propia Descripción, la cual puede ser modificada según se detalla con el método correspondiente más adelante. 
</p>
<p style="text-align: justify;">
Un atributo importante de las Cuentas de Ahorro es "enable" el cual indica si la Cuenta está Habilitada (true) o Deshabilitada (false). En este último caso, no se podrán realizar operaciones sobre la cuenta.
</p>

**Request Body:**
```json
{
  "description": "Mi cuenta description" 
}
```

**Response:**
```json
{
    "className": "FinancialSavingAccount",
    "id": "hwie7J7ssTbCUgK7T9wPgCN_1dsvOmbgMlSvUEya4nYbHe3PNFSOsn1mC85Koo8a6R9kr4bxD8pp_k9Y74yeiw",
    "name": "Mi cuenta description",
    "description": "Pesos Argentinos",
    "isDefault": false,
    "balanceAmount": 0.00,
    "arrangedAmount": 0.00,
    "overdraftCredit": 0.00,
    "availableCredit": 0.00,
    "enable": true,
    "product": {
        "code": "$",
        "id": "05C940B841BB20A070FC",
        "name": "Pesos Argentinos"
    },
    "creationDate": "2021-01-20T17:17:30-03:00"
}
```

------

###### Consulta de Cuentas de Ahorro

<p style="text-align: justify;">
Para consultar las Cuentas de Ahorro asociadas a una Cuenta Financiera (Financial Account) se debe utilizar el siguiente método: 
</p>

- **GET /financial-accounts/saving-accounts**
<p style="text-align: justify;">
El método responderá un array con todas las Cuentas de Ahorro asociadas a dicha Cuenta Billetera con la información correspondiente a cada una de ellas.
</p>
<p style="text-align: justify;">
Si en el llamado se especifica el parámetro "enable", el array devuelto sólo contendrá las que se correspondan con dicho parámetro. De esta manera se podrían consultar SOLO las cuentas Habilitadas (enable=true). Por default, si NO se especifica parámetro, se toma el valor "true"
</p>

**Response:**
```json
[
    {
      "className": "FinancialSavingAccount"
      "id":"hwie7J7ssTbCUgK7T9wPgCN_1dsvOmbgMlSvUEya4nYbHe3PNFSOsn1mC85Koo8a6R9kr4bxD8pp_k9Y74yeiw",
      "name": "Mi cuenta description",
      "description": "Pesos Argentinos",
      "isDefault": false,
      "balanceAmount": 0.00,
      "arrangedAmount": 0.00,
      "overdraftCredit": 0.00,
      "availableCredit": 0.00,
      "enable": true,
      "product": {
          "code": "$",
          "id": "05C940B841BB20A070FC",
          "name": "Pesos Argentinos"
      },
      "creationDate": "2021-01-20T17:17:30-03:00"
    }
]
```

---------

###### Consulta de cuentas Disponibles (Available Accounts)

- **GET /financial-accounts/available-accounts**
<p style="text-align: justify;">
La invocación de este método permite obtener una respuesta en forma de array de todas las Available Accounts (cuentas disponibles).
</p>
<p style="text-align: justify;">
Sólo devuelve las cuentas de tipo available.
</p>

**Response:**
```json
[
    {
      "className": "FinancialAvailableAccount",
      "id":"MvTxNsOS91P0oAm8Slvpj0470xmjMUh0MlSvUEya4nYbHe3PNFSOsp68lRvviEQEiTDIQuKnKRBTbq5_ohsXvg",
      "name": "Disponible",
      "description": "Pesos Argentinos",
      "isDefault": true,
      "balanceAmount": 1304.86,
      "arrangedAmount": 0.00,
      "overdraftCredit": 0.00,
      "availableCredit": 0.00,
      "enable": true,
      "product": {
        "code": "$",
        "id": "05C940B841BB20A070FC",
        "name": "Pesos Argentinos"
      }
    }
]
```

------

###### Consulta de Detalle de Balance de Cuentas de Ahorro

<p style="text-align: justify;">
Una vez identificado el ID de la Cuenta de Ahorro que se quiere consultar, se puede, mediante el siguiente método, consultar el detalle del Balance de la Cuenta entre una fecha desde y una fecha hasta (parámetros NO obligatorios).
</p>

- **GET /financial-account/{id}/balance-detail**
<p style="text-align: justify;">
(donde ID es el id de la Cuenta obtenido desde la Consulta de Cuentas de Ahorro)
</p>
<br>

<p style="text-align: justify;">
Mediante este método se puede conocer el detalle de los movimientos agrupados entre una fecha desde y una fecha hasta. Es decir, cómo se llegó desde el Saldo de la cuenta en la fecha desde hasta el Saldo de la fecha hasta. En caso de necesitar mayor detalle en los movimientos, se deberá utilizar el método de consulta de movimientos de stocks.
</p>

<p style="text-align: justify;">
En la respuesta podremos encontrar un array (balanceDetails) con la lista de movimientos que tuvo esa Cuenta de Ahorro y los valores de Saldo Inicial (beginAmount) y Saldo Final (endAmount) de dicho período.
</p>
<p style="text-align: justify;">
Los movimientos que devuelve esta consulta, se encuentran agrupados según ciertos parámetros, a saber, agrupamiento por tipo de movimiento (ingreso/egreso), por concepto (bruto, comisión, impuesto, etc) y por transacción origen que lo genera (banktransfer, inner transfer, etc).
</p>

**Response:**

```json
{
  "beginDate": "20201129",
  "endDate": "20210120",
  "beginAmount": 0.00,
  "endAmount": 4401.86,
  "pendingAmount": 0.00,
  "rejectedAmount": 0.00,
  "balanceDetails": [
    {
      "date": "20210120",
      "totalAmount": -4416.00,
      "movementSubType": "GrossAmount",
      "transactionType": "P2pInputTransfer",
      "name": "Transferencia Ingreso PtP",
      "description": ""
    },
    {
      "date": "20210120",
      "totalAmount": 16.14,
      "movementSubType": "GrossAmount",
      "transactionType": "InternalProductTransfer",
      "name": "Transferencia interna Producto",
      "description": ""
    },
    {
      "date": "20210120",
      "totalAmount": -2.00,
      "movementSubType": "GrossAmount",
      "transactionType": "InternalProductTransfer",
      "name": "Transferencia interna Producto",
      "description": ""
    }
  ]
}

```
------

###### Consulta de Movimientos de Stocks de una Cuentas de Ahorro

<p style="text-align: justify;">
Una vez identificado el ID de la Cuenta de Ahorro que se quiere consultar, se puede, mediante el siguiente método, consultar los movimientos de la Cuenta. A dicha consulta se le pueden añadir ciertos parámetros para poder refinar la respuesta, aunque no son datos obligatorios. En caso de NO enviar ningún parámetro, la respuesta serán todos los movimientos de la cuenta.
</p>

- **GET /financial-account/{id}/movements**
<p style="text-align: justify;">
(donde ID es el id de la Cuenta obtenido desde la Consulta de Cuentas de Ahorro)
</p>

> **NOTA:** A diferencia del método Activities (donde devuelve TODOS los movimientos de la Cuenta Billetera o principal), este método solo devuelve los movimientos de stocks específicos de la Cuenta de Ahorro cuyo ID se pasa por parámetro.

<p style="text-align: justify;">
Cabe aclarar que una transacción puede generar más de un movimiento de stock, por ejemplo impuestos, comisiones, etc, en este caso, este método devolverá cada uno de esos movimientos de stock dentro del array.
</p>

**Response:**
```json
[
  {
    "id": "50840010151826747B0E",
    "created": "2021-01-20T17:24:13-03:00",
    "confirmationDate": "2021-01-20T17:24:13-03:00",
    "amount": 4.14000000,
    "description": "Transferencia interna Producto - 400837 - ",
    "comments": "Holaa",
    "state": "approved",
    "stateDetail": {
      "message": "Confirmada",
      "transactionType": "Sale"
    },
    "transactionType": "Sale",
    "movementSubType": "GrossAmount",
    "transactionType": "InternalProductTransfer"
  }
]
```
------

###### Cierre de una Cuentas de Ahorro

<p style="text-align: justify;">
Para realizar el cierre de una Cuenta de Ahorro se debe utilizar el siguiente método:
</p>

- **POST /financial-account/saving-account/{id}/close**

> **Consideraciones:**
> - Las cuentas de ahorro SOLO pueden ser cerradas si poseen Saldo = 0 (cero)
> - Solamente se desactiva el objeto Cuenta de Ahorro. El objeto seguirá existiendo, y se podrá seguir obteniendo sus reportes, movimientos y saldos, para fechas tardías.

**Response:**
```json
{    
  "className": "FinancialSavingAccount",
  "id": "hwie7J7ssTbWy6ZhUM1w5DkX07lgCsi6MlSvUEya4nYbHe3PNFSOsn1mC85Koo8a3NRVUlArB-dR8pPKPCaeVQ",
  "name": "nueva caja",
  "description": "Pesos Argentinos",
  "isDefault": false,
  "balanceAmount": 0.00,
  "arrangedAmount": 0.00,
  "overdraftCredit": 0.00,
  "availableCredit": 0.00,
  "enable": false,
  "product": {
    "code": "$",
    "id": "05C940B841BB20A070FC",
    "name": "Pesos Argentinos"
  },
  "creationDate": "2021-01-20T12:21:05-03:00",
  "closeDate": "2021-01-20T12:24:59-03:00"
}

```

------

###### Gestión de Transferencias en Cuentas de Ahorro

<p style="text-align: justify;">
Las transferencias de dinero entre la Cuenta Billetera (o principal) y las Cuentas de Ahorro se podrán gestionar mediante los siguientes métodos:
</p>

- **POST /transfer/inner**
<p style="text-align: justify;">
Con este endpoint podemos transferir dinero entre la cuenta principal y las cuentas de ahorro
</p>
<br>

- **Request Body:**
```json
{
  "Origin": "{{financial account id de origen}}",
  "Destination": "{{financial account id de destino}}",
  "amount": 4.14,
  "clientTransactionId": "7e1b6677-a164-4fa4-9e8f-3c65678403f6",
  "transactionType": "Transfer",
  "comments": "holaaa",
  "transactionData":{
   "origin":"hwie7J7ssTbCUgK7T9wPgCN_1dsvOmbgMlSvUEya4nYbHe3PNFSOsp68lRvviEQEd3DcJhK2Pti4nftsLQ5Uig",
  "destination":"hwie7J7ssTbCUgK7T9wPgCN_1dsvOmbgMlSvUEya4nYbHe3PNFSOsn1mC85Koo8a6R9kr4bxD8pp_k9Y74yeiw"  
  }
}
```

**Response:**
```json
{
  "id": "4971001015063897E247",
  "number": 400837,
  "created": "2021-01-20T17:24:13-03:00",
  "confirmationDate": "2021-01-20T17:24:13-03:00",
  "amount": 4.14,
  "description": "Transferencia interna Producto - 400837 - ",
  "comments": "holaaa",
  "state": "approved",
  "stateDetail": {
      "message": "Confirmada",
      "transactionType": "InternalProductTransfer"
  },
  "transactionType": "InternalProductTransfer",
  "clientTransactionId": "7e1b6677-a164-4fa4-9e8f-3c65678403f6",
  "confirmationId": "400837",
  "transactionData":{
 "destination":"hwie7J7ssTbCUgK7T9wPgCN_1dsvOmbgMlSvUEya4nYbHe3PNFSOsn1mC85Koo8a6R9kr4bxD8pp_k9Y74yeiw",
    "classname": "TransferInnerData",
    "originDescription": "Disponible",
    "origin": "hwie7J7ssTbCUgK7T9wPgCN_1dsvOmbgMlSvUEya4nYbHe3PNFSOsp68lRvviEQEd3DcJhK2Pti4nftsLQ5Uig",
    "destinationDescription": "Mi cuenta description"
    }
}
```

-------

###### Consultar Transferencias: 

<p style="text-align: justify;">
En caso de requerir consultar los datos de una transacción en particular se deberá utilizar el siguiente método: 
</p>

- **GET /transfer/{Id}**
<p style="text-align: justify;">
(donde ID es el id de la transacción obtenido desde la Consulta anterior)
</p>

**Response:**
```json
{
  "id": "4971001015063897E247",
  "number": 400837,
  "created": "2021-01-20T17:24:13-03:00",
  "confirmationDate": "2021-01-20T17:24:13-03:00",
  "amount": 4.14,
  "description": "Transferencia interna Producto - 400837 - ",
  "comments": "holaaa",
  "state": "approved",
  "stateDetail": {
    "message": "Confirmada",
    "transactionType": "InternalProductTransfer"
  },
  "transactionType": "InternalProductTransfer",
  "clientTransactionId": "7e1b6677-a164-4fa4-9e8f-3c65678403f6",
  "confirmationId": "400837",
  "transactionData": {
 "destination":"hwie7J7ssTbCUgK7T9wPgCN_1dsvOmbgMlSvUEya4nYbHe3PNFSOsn1mC85Koo8a6R9kr4bxD8pp_k9Y74yeiw",
      "classname": "TransferInnerData",
      "originDescription": "Disponible",
      "origin":"hwie7J7ssTbCUgK7T9wPgCN_1dsvOmbgMlSvUEya4nYbHe3PNFSOsp68lRvviEQEd3DcJhK2Pti4nftsLQ5Uig",
      "destinationDescription": "Mi cuenta description"
    }
}
```

------

###### Ejemplo de Caso de USO - Metas de Ahorro 

<p style="text-align: justify;">
El concepto de Metas de Ahorro es brindarle al usuario de la Billetera la posibilidad de generar un Cuenta de Ahorro (tipo alcancía), con una descripción asociada (Objetivo del Ahorro, por ej: "Comprar una Bicicleta"), una monto máximo,  donde pueda ir transfiriendo dinero desde su cuenta Principal a la Cuenta de Ahorro hasta llegar a la meta definida. 
</p>
<br>

**Las Metas de Ahorro tendrán las siguientes características:**
<br>
1. <p style="text-align: justify;">
Las metas de ahorro se definirán a partir de la utilización de sub cuentas. La creación de la sub cuenta  se hará bajo demanda es decir que no será realizada automáticamente a partir del registro de una billetera sino a partir de una billetera creada mediante la acción del propio usuario. 
</p>
2. <p style="text-align: justify;">
Las metas de ahorro tienen una sola especie que es PESOS. 
</p>
3. <p style="text-align: justify;">
Solo puede haber una meta de ahorro por Billetera en forma simultánea (si bien la Plataforma lo permite, en este caso de uso, solo hay una)
</p>
4. <p style="text-align: justify;">
Las metas de ahorro no pueden usar el Saldo que tienen para ningún pago, recarga o transferencias internas ni externas.
</p>
5. <p style="text-align: justify;">
Las metas de ahorro solo pueden transferir desde y hacia la cuenta principal de la billetera, no hay límites ni mínimos definidos en la cantidad de movimientos que se pueden hacer entre la meta de ahorro y la cuenta principal de la billetera.
</p>
6. <p style="text-align: justify;">
La aplicación wallet será responsable de: administrar los límites (meta objetivo) y las categorías definidas para la meta dado que esa funcionalidad no existe del lado de la Plataforma OpenPass.
</p>
7. <p style="text-align: justify;">
La aplicación wallet,en caso de ser necesario, será la responsable de medir el grado de alcance de la meta de ahorro (límite vs saldo).
</p>
8. <p style="text-align: justify;">
La subcuenta creada podrá tener una descripción y la misma podrá ser persistida por bhub.
</p>
9. <p style="text-align: justify;">
La billetera que necesite tener una nueva meta de ahorro deberá esperar a terminar la existente.
</p>

10. <p style="text-align: justify;">
No se prevén acciones ni control de lado de la Plataforma OpenPass cuando una meta llega a su objetivo, puesto que no se controla del lado de la Plataforma.
</p>
 
 
**Resolución del Caso de Uso:**

- El usuario da de alta una Meta de Ahorro
- Le asigna una Descripción y define el monto objetivo
- Una vez generada la Meta de Ahorro (en la Plataforma OpenPass sería una - Cuenta de Ahorro), el usuario puede comenzar a "Ingresar Dinero" desde su saldo de billetera.
- También puede "Sacar Dinero" en caso que lo considere necesario, en cuyo caso el dinero volverá al saldo de billetera. ( en la Plataforma OpenPass estos movimientos corresponden a Transferencias)























