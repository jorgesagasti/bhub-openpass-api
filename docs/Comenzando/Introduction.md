# Introduction

OpenPass' Core Fintech, Cash Management & White Label Mobile Wallet Platform provides the means to develop an end-to-end digital payment experience. It also frees financial services from their traditional limitations as OpenPass has already done the hard work; thus, enabling a fast and affordable setup and deployment of digital banking, payment infrastructure, and card solution, among others.

The OpenPass Platform connects with different service providers (e.g., Top-Up Networks, Card Processors, API Banks, etc.), which have different processes, services, flows, criteria, and circuitry to perform the corresponding operations. By using the OpenPass Platform, the differences between providers are unified and consolidated and the Platform's own services are exposed.

Whoever integrates with the Platform should do so with the services exposed by OpenPass and the platform itself will solve how to interact with each provider as defined in the business rules. You do not have to worry about integration.
Integrate with networks and card issuers to create and activate cards, manage accounts, make top-ups, pay services, interact with the banking system, authorize transactions, make secure transactions, and more.

OpenPass provides a modular approach whereby customers can leverage an entire technology stack or its components individually: white-label UX/UI interfaces, middleware and orchestration services, core fintech and issuance, acquisition and processing of card transaction, etc.

We have an extensive API library to support development teams and accelerate the solutions implementations of core fintech, payment infrastructure solutions, card solutions and all the necessary functionalities.

Through this portal, you will gain access to the documentation of every exposed service which can be done in a simple, agile, and unified way.

The documentation will help you understand the different solutions provided by the OpenPass Platform so that you can incorporate them into you products and services.
