# Starter´s Guide

Here are some recommendations to improve the integration experience with OpenPass APIs.

- Discover.

Explore our documentation portal and the extensive set of APIs available. Check the documentation and "play" in our Sandbox environment.

- Ask for your developer account.

Contact the Integrations team by email: integracion@openpass.com.ar so that they can guide you through the necessary steps to get your developer account.

- Create a project and take your ideas to the next level with our APIs.

Once your account is created, the integrations team will give you access to the Sandbox environment so you can test the APIs of your choice.

- Ready for the next level?
Once you are ready to step into the next environment (UAT or production), the Implementations Team will guide you through the following steps.
