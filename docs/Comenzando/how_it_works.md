# How it works

Through our APIs, we provide our modules and their functionalities so that you can create your own solutions and applications. We are aware of your time and work. Thus, our services are based upon the best available standards, high security and a set of proven APIs.

### API Authentication

Once you have a developer account and its credentials, you will get an access token through the API {POST /login}; this token will be sent with every call made to the OpenPass API.

### API Protocols

The OpenPass API use POST/GET/PUT requests for the communication and HTTP response code to indicate status and/or errors. Every response follows the JSON notation.
The OpenPass API are enabled through HTTPS TLS v1.2 to ensure data privacy.

### API Referral

OpenPass has a set of APIs sorted by product categories:

 - Core API
 
 - Banking Services API
 
 - Cards API
 
 - Payment Link API
 
 - Top-ups API
 
 - Bill Payments API
 
 - Notifications API
 
 - WebHooks
