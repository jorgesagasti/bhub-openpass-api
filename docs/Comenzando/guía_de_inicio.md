# Guía de Inicio

A continuación le dejamos algunas recomendaciones para mejorar la experiencia de integración con las APIs de OpenPass.



- Descubra.

Explore nuestro portal de documentación y el amplio conjunto de APIs disponibles. Revise la documentación y "juegue" en nuestro entorno de Sandbox.

- Solicite su cuenta de desarrollador.

Póngase en contacto con el equipo de Integraciones al mail: integracion@openpass.com.ar para que puedan guiarlo en los pasos necesarios para obtener su cuenta de desarrollador.

- Cree un proyecto y lleve sus ideas al siguiente nivel con nuestras APIs.

Una vez creque se crea su cuenta, el equipo de integraciones le dará acceso al entorno de Sandbox para que pueda probar las APIs que desee.

- ¿Listo para pasar al siguiente nivel?

Una vez que esté listo para pasar al siguiente entorno (UAT o producción), el equipo de Implementaciones lo acompañará y guiará en los pasos necesarios.

