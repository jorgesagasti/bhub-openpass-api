# Cómo funciona

A través de nuestras APIs, ponemos a disposición todas nuestros módulos y sus funcionalidades para que pueda crear sus propias soluciones y aplicaciones. Somos conscientes del valor de su tiempo y trabajo, es por eso que nuestros servicios se basan en los mejores estándares disponibles, alta seguridad y un conjunto probado de APIS. 

### Autenticación API

Una vez que tenga una cuenta de desarrollador y sus credenciales debe obtener un token de acceso a través de la API _{POST /login}_; este token se enviará con cada llamada realizada a las API de OpenPass.

### Protocolos API

Las API de OpenPass utilizan solicitudes POST/GET/PUT para la comunicación y códigos de respuesta HTTP para indicar el estado y / o errores. Todas las respuestas siguen la notación JSON.

Las API de OpenPass están habilitadas a través de HTTPS TLS v1.2 para garantizar la privacidad de los datos.

### Referencia de API

OpenPass tiene un conjunto de API clasificadas por categorías de productos:

 - API Core
 
 - API de Servicios Bancarios
 
 - API de Tarjetas
 
 - API de Link de Pagos
 
 - API de Recargas
 
 - API de Pago de Servicios
 
 - API de Notificaciones
 
 - WebHooks