# Headers

The following Header must be added into each request made to OpenPass' APIs.
The HTTP allows the client and server to send additional information along with the request.


Name | Description | Type | Mandatory?
---------|----------|---------|---------
 applicationId | - | string | si 
 deviceld | - | string | si
