# Headers

El siguiente Header debe agregarse en cada solicitud realizada a las API de OpenPass.

El HTTP permite al cliente y al servidor enviar información adicional con la solicitud o respuesta.


Nombre  | Descripción | Tipo | Mandatorio?
---------|----------|---------|---------
ApplicationId | ApiKey que identifica la aplicación que  envía el request, este ID es provisto por OpenPass | string | si
DeviceId | Es el ID que identifica el dispositivo que envía el Request. | string | si
AccessToken |  identificador temporal y único con permisos asociados para el uso y acceso a las apis | string | -