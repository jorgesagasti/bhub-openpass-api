# Integration support

OpenPass offers a 24x7 Support service for Integrations:

- Monday to Friday from 9 am to 18 pm: Technical Support, Commercial Assistance, and Transactional Support.

### Contact

- Implementations: integraciones@openpass.com.ar

- Technical Support: soporte@openpass.com.ar

- Other requests: info@openpass.com.ar
