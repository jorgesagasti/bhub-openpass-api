# *TRX-PULL*

### *¿Qué son las Tranferencias Pull?*

Las Transferencias Pull son solicitudes o pedidos de fondos que permiten mediante el débito de la cuenta (a la vista o de pago) del cliente receptor de la solicitud y previa autorización o consentimiento, la acreditación inmediata de fondos en la cuenta del cliente solicitante.