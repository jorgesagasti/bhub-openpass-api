# Errores

Para determinar el estado de cualquier llamada a la API de OpenPass, utilice la combinación del código de estado HTTP devuelto en la respuesta, el código de error de OpenPass o la plataforma y otra información incluida en el mensaje de error para aplicar la lógica de procesamiento correcta. 

La siguiente tabla proporciona orientación adicional sobre cómo interpretar y actuar sobre las respuestas de estado.

### Anexo de tabla de códigos de error de validación de la plataforma OpenPass

**[000] Respuestas para casos de éxito**

ErrorCode | ErrorDescriptión | ErrorDetail
---------|----------|-----------------------
 0 | Transacción realizada con éxito | La transacción se realizó correctamente, en los casos de transacciones de ventas, indica satisfactoriamente que la operación está confirmada y se afectó el saldo de la cuenta. 

 **[1xx] Respuestas para para problemas de seguridad**

 ErrorCode | ErrorDescriptión | ErrorDetail
---------|----------|-----------------------
 101 | Usuario y/o password inválidos | Validar si los datos de usuario y pasword son correctos.
 102 | Cuenta bloqueada | La cuenta (bolsa de saldo) esta inactiva o bloqueada, en ambos casos consultar con Bhub.
 103 | Cliente/cuenta incorrectos | Validar los 4 datos que conforman el esquema de seguridad de la conexión.
 104 | Usuario bloqueado | El usuario de acceso al sistema está inactivo o bloqueado, en ambos casos consultar con Bhub.
 105 | Usuario no habilitado | El usuario de acceso al sistema se encuentra inhabilitado. Consultar con Bhub.
 106 | Sin permisos suficientes | El usuario de acceso al sistema no posee los permisos necesarios para continuar. Consultar con Bhub.
 107 | Usuario dado de baja | 
 108 | Cuenta pendiente de baja | 
 150 | Dispositivo bloqueado | Dispositivo bloqueado.
 151 | Objeto no disponible | 
 152 | Device invalido |
 153 | Cuenta suspendida |

 **[2xx] Respuestas a problemas de conexión o sistemas**

ErrorCode | ErrorDescriptión | ErrorDetail
---------|----------|-----------------------
 201 | Error del servidor de plataforma | La transacción no terminó satisfactoriamente, en el caso de las ventas implica que la operación debe ser consultada por el servicio de consulta de estado para validar su estado final.
 202 | Proveedor no disponible | La transacción no terminó satisfactoriamente por inconvenientes en el proveedor del producto. Debe considerarse estado final e intentar con nueva transacción.

 **[3xx] Respuestas a problemas de conexión o sistemas** 

 ErrorCode | ErrorDescriptión | ErrorDetail
---------|----------|-----------------------
 301 | Transacción inexistente | La transacción que se está intentando consultar por nroTransaccionExt no existe en los registros del sistema, por ende no afecto el saldo de la cuenta.
 302 | Código de producto incorrecto | Se está intentando operar con un código de producto que no existe, ver anexo para validar productos disponibles.
 303 | Datos de recarga incorrectos | Obtendrá este error cuando el dato de recarga no posea el formato correcto definido, o bien no sea un abonado válido para el proveedor o el abonado esté inhabilitado para recibir saldo virtual.
 304 | Importe incorrecto | El importe es incorrecto, esto solo podría suceder en casos de importe que exceden los límites del proveedor pero no los definidos para el producto.
 305 | Importe inferior al mínimo permitido | Los proveedores tienen un rango definido para el importe de sus recargas. En caso de no estar dentro del mismo obtendrá uno de estos errores.
 306 | Importe superior al máximo permitido | Los proveedores tienen un rango definido para el importe de sus recargas. En caso de no estar dentro del mismo obtendrá uno de estos errores.
 307 | Nro. de transacción duplicado | Obtendrá este error si envía un NroTransaccionExt duplicado.
 308 | Transacción repetida | Obtendrá este error si envía mas de una transacción con el mismo datoRecarga y el mismo importe dentro de un lapso de diez minutos.
 309 | Proveedor no disponible | No se pudo realizar la conexión interna con el proveedor del producto.
 310 | Saldo virtual insuficiente | La plataforma no posee el saldo necesario para vender el producto solicitado.
 311 | Producto no habilitado | Indica que el producto que está tratando de comercializar no se encuentra disponible para este usuario.
 312 | Transacción no habilitada | Indica el tipo de transacción que está tratando de realizar no se encuentra habilitada para este usuario.
 313 | Sin stock para el producto solicitado | No hay stock disponible para la venta.
 321 | Código incorrecto |  
 322 | Datos transacción incorrectos |
 323 | Sesión transaccional invalida | 
 324 | Operacion no permitida | 
 325 | Acción ya aplicada | 
 326 | Parámetros inválidos | 

 **[3xx] Por depósitos** 

 ErrorCode | ErrorDescriptión | ErrorDetail
---------|----------|-----------------------
 314 | Origen de Depósitos | Forma de pago incorrecta. 
 315 | Origen de Depósitos | Sucursal incorrecta.
 316 | Origen de Depósitos | Cuenta bancaria incorrecta.
 317 | Origen de Depósitos | Número operación incorrecto.
 318 | Origen de Depósitos | Tarjeta bancaria incorrecta.
 319 | Origen de Depósitos | Precio del incorrecto.
 320 | Origen de Depósitos | Método de pago incorrecto.

 **[3xx] Por cobro facturas**

 ErrorCode | ErrorDescriptión | ErrorDetail
---------|----------|-----------------------
 330 | Empresa cobro incorrecta | 
 331 | Código de barras no encontrado | 
 332 | Identificador de cliente no encontrado |  

 **[3xx] Por scoring**

 ErrorCode | ErrorDescriptión | ErrorDetail
---------|----------|-----------------------
 340 | Cuenta sin scoring |
 342 | Importe scoring por transacción fuera de rango |
 343 | Importe scoring diario fuera de rango |
 344 | Cantidad scoring diario fuera de rango |
 345 | Importe scoring mensual fuera de rango |
 346 | Cantidad scoring mensual fuera de rango |

**[3xx] Errores del SSO y Payment**

ErrorCode | ErrorDescriptión | ErrorDetail
---------|----------|-----------------------
350 | Missing Account or User Account
351 | Invalid external id
352 | Missing account tributary code
353 | Missing account business name
354 | Duplicated username or email
355 | Missing client transaction id
356 | Missing payment intent
357 | Missing provider payment intent
358 | Missing provider id payment intent
359 | Missing provider business name payment intent
360 | Missing customer payment intent
361 | Missing customer identification number payment intent
362 | Missing customer provider identification number payment intent

**[3xx] Error anulación genérico**

ErrorCode | ErrorDescriptión | ErrorDetail
---------|----------|-----------------------
399 | Genérico

**[4xx] Errores que implican demoras factibles de consultas posteriores del estado**

ErrorCode | ErrorDescriptión | ErrorDetail
---------|----------|-----------------------
401 | Estado transacción | Transacción en proceso.
402 | Estado transacción | Transacción anulada.
403 | Estado transacción | Transacción pendiente de confirmación.
404 | Estado transacción | Transacción en curso.

**[5xx] Errores de tipo acción o validaciones**

ErrorCode | ErrorDescriptión | ErrorDetail
---------|----------|-----------------------
501 | Acción / validación | Transacción no confirmada.
502 | Acción / validación | Tipo cuenta incorrecto.
503 | Acción / validación | Lista de precios incorrecta.
504 | Acción / validación | Código usuario en uso.
505 | Acción / validación | Código usuario incorrecto.
506 | Acción / validación | Tipo reclamo incorrecto.
507 | Acción / validación | Fecha incorrecta.
508 | Acción / validación | Configuración incorrecta.
509 | Acción / validación | Hora incorrecta.
510 | Acción / validación | Informacion adicicional cliente.
511 | Acción / validación | Tipo negocio incorrecto.

**[6xx] Errores de tipo genérico**

ErrorCode | ErrorDescriptión | ErrorDetail
---------|----------|-----------------------
601 | Validaciones incorrectas | Validaciones incorrectas.

**Módulo tarjetas**

ErrorCode | ErrorDescriptión | ErrorDetail
---------|----------|-----------------------
147001 | CuentaTarjeta no operativa para transaccionar
147002 | Tarjeta no operativa para transaccionar


**Remote Invocation Errors**

ErrorCode | ErrorDescriptión | ErrorDetail
---------|----------|-----------------------
120001 | Error invocando a ServicioExterno
127002 | Timeout invocando a ServicioExterno


**Token Errors**

ErrorCode | ErrorDescriptión | ErrorDetail
---------|----------|-----------------------
701 | Token Invalido