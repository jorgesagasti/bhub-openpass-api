# Error treatment


### Component error treatment sequence scheme



![](../../assets/images/secuencias_tratamiento_de_errores_por_componente.png)


> In the absence of a response (Time Out) or the response of a pending state, it is recommended to immediately act with the corresponding Get Transaction and at intervals of 5/10 seconds until a final transaction state is obtained.


##### The OpenPass Platform treats errors two ways


*   Platform validation errors
*   Messaging errors - Nested transaction

### Validating errors - Platform

<p style="text-align: justify;">
OpenPass como plataforma transaccional tiene responsabilidades sobre los datos que administra, en tal sentido cuando se produce la activación de una validación de la plataforma OpenPass se genera la devolución de un error. 
As a transactional platform, OpenPass has responsibilities over the data it manages. Thus, when the activation of a validation of the OpenPass platform occurs, an error is generated.
</p>

<p style="text-align: justify;">
The cases in which the error report is due to control of the OpenPass platform, it is possible to identify the error with the structure detailed below. These errors are reported as HTTP 400.
</p>

Some errors for which the OpenPass platform is responsible for are:

  - **User validation enabled**      → `errorCode 105`
  - **Account balance validation**      → `errorCode 301`

</br>
<p style="text-align: justify;">
These errors, like the rest of those identified by the OpenPass platform, are always typified by an ErrorType and an ErrorCode, some examples are: (**See annex table of validation error codes of the OpenPass platform**)
</p>

**Response:** HTTP Status 400
```json
{

    "code": "2",

    "message": "request error",

    "data": {

        "messageText": "User not enabled",

        "errorCode": 105,

        "errorType": "Security"

    }

}
```

**Response:** HTTP Status 400
```json
{

    "code": "2",

    "message": "request error",

    "data": {

        "messageText": "Insufficient virtual balance.",

        "errorCode": 310,

        "errorType": "Business"

    }

}
```

### Messaging errors - Nested transaction

<p style="text-align: justify;">
If the initial validations made by the OpenPass platform are passed, the request is made to the different products. Products can be offered by aggregators (supplier hubs) or directly by each particular supplier.
</p>
Within the messaging, the first thing to evaluate is the overall status of the transaction. These states can be:


  - **Approved**:  corresponds to a final transaction status. Exemplifying in the first sequence of the diagram.

  - **Rejected**: corresponds to a final transaction status. Exemplifying in the first sequence of the diagram.

  - **Pending**: corresponds to an intermediate state of transaction that will eventually result in an approved or rejected. The length of time a transaction remains in this state is variable and depends on the type of product traded. Exemplifying in the first sequence of the diagram.

</br>

###### For example:

**Response:** HTTP Status 200
```json
    "state": "pending",

    "stateDetail": {

        "message": "Error de timeout [45]",

        "transactionType": "Sale"

    },

    "id": "2D060010153722F792D7",

    "number": 675870517,

    "created": "2020-09-18T14:04:57-03:00",

    "amount": 10.00000000,

    "description": "Recharge - 675870517",

    "state": "pending",

    "stateDetail": {

        "message": "Error de timeout [45]",

        "transactionType":** "Sale"**

    },
```



### Transactional States

<p style="text-align: justify;">
Transactional states are determined by the supplier, either a buncher or the product supplier itself . Since there is no general nomenclature of status codes agreed by all suppliers/product, and since this point is completely heterogeneous and beyond the scope of the platform, there is no cross provider code mapping. So, the OpenPass platform resolved this particular situation by including the “message or text” as detailed by the provider, adding the typification of the corresponding transaction type at the level of detail of the transaction.
</p>

**Response:** HTTP Status 200
```json
{

"state": "rejected",

"stateDetail": {

        "message": "Timeout error [45]",

        "transactionType":** "Sale"**

    },

}
```

> Any transaction capable of being validated and processed by the OpenPass platform will be returned with Status 200 regardless of the transactional status of the business.

<p style="text-align: justify;">
Another consideration that the OpenPass platform makes when returning the status is working with nested transactions, such as the payment of a product through a credit card.
</p>

<p style="text-align: justify;">
In this case, the original request of the wallet will determine two transactions: one to debit the card and another to notify the payment to the product. This situation can cause for one or two open transactions to suffer an error. In that case, the OpenPass platform will take the first error returned and it will be this one that is finally returned to the middleware.
</p>

This point is especially clarified since no collection or array of transactional errors is returned.
