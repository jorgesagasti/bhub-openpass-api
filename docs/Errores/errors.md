# Errors

To determine the status of any OpenPass API call, use the combination of the HTTP status code returned in the response, the OpenPass or platform error code, and other information included in the error message to apply the correct processing logic.
 
The following table provides additional guidance on how to interpret and act on status responses.
 
## Annex to the table of validation error codes for the OpenPass platform

**[000] Answers for success cases**

ErrorCode | ErrorDescriptión | ErrorDetail
---------|----------|-----------------------
0 | Successfully completed transaction | The transaction was successful. In cases of sales transactions, it successfully indicates that the operation is confirmed and the account balance was affected.

**[1xx] Answers for security issues**

ErrorCode | ErrorDescriptión | ErrorDetail
---------|----------|-----------------------
101 | Invalid username and/or password | Validate if the user and password data are correct.
102 | Blocked account | The account (balance exchange) is inactive or blocked. In both cases, you should consult with Bhub.
103 | Incorrect customer/account | Validate the 4 data that make up the connection security scheme.
104 | Blocked user | The user accessing the system is inactive or blocked. In both cases, you should consult with Bhub.
105 | User not enabled | The access user to the system is disabled. Consult with Bhub.
106 | Without sufficient permissions | The user accessing the system does not have the necessary permission to continue. Consult with Bhub.
107 | User unsubscribed
108 | Account pending of unsubscription
150 | Locked device | Locked device.
151 | Object not available
152 | Invalid device
153 | Suspended account

**[2xx] Responses to connection or systems problems**

ErrorCode | ErrorDescriptión | ErrorDetail
---------|----------|-----------------------
201 | Platform server error | The transaction did not end satisfactorily. In the case of sales, it implies that the operation must be consulted by the status query service to validate its final status.
202 | Provider not available | Transaction did not end satisfactorily due to inconveniences in the product provider. It should be considered final state and try with a new transaction.

**[3xx] Responses to connection or systems problems**

ErrorCode | ErrorDescriptión | ErrorDetail
---------|----------|-----------------------
301  | Non-existent transaction | The transaction that you are trying to query by nroTransaccionExt does not exist in the system logs. Thus, it did not affect the account balance.
302 | Incorrect product code | You are trying to operate with a product code that does not exist. See annex to validate available products.
303 | Incorrect top-up data | You will get this error when the top-up data does not have the correct format defined, or is not a valid subscriber for the provider or the subscriber is disabled to receive virtual balance.
304 | Incorrect amount | The amount is incorrect. This could only happen in cases of amount exceeding the limits of the provider, but not those defined for the product.
305 | Amount less than the minimum allowed | Providers have a defined range for the amount of their top-ups. In case you are not within the range, you will get one of these errors.
306 | Amount less than the maximum allowed | Providers have a defined range for the amount of their top-ups. In case you are not within the range, you will get one of these errors.
307 | No. of duplicate transaction | You will get this error if you send a duplicate NroTransaccionExt.
308 | Repeated transaction | You will get this error if you send more than one transaction with the same chargeback data and the same amount within ten minutes.
309 | Provider not available | The internal connection to the product provider could not be made.
310 | Insufficient virtual balance | The platform does not have the necessary balance to sell the requested product.
311 | User not enabled | Indicates that the product you are trying to market is not available for this user.
312 | Transaction not enabled | Indicates that the type of transaction you are trying to perform is not enabled for this user.
313 | Out of stock of the requested product | There is no stock available for sale.
321 | Incorrect codeIncorrect code 
322 | Incorrect transaction data
323 | Invalid transactional session

**[3xx] Deposits**

ErrorCode | ErrorDescriptión | ErrorDetail
---------|----------|-----------------------
314 | Origin of Deposits | Incorrect payment method.
315 | Origin of Deposits | Wrong branch.
316 | Origin of Deposits | Incorrect bank account.
317 | Origin of Deposits | Incorrect operation number.
318 | Origin of Deposits | Incorrect bank card.
319 | Origin of Deposits | Incorrect price.
320 | Origin of Deposits | Incorrect payment method.

**[3xx] Collection invoices**

ErrorCode | ErrorDescriptión | ErrorDetail
---------|----------|-----------------------
330 | Incorrect collection company
331 | Barcode not found
332 | Customer ID not found

**[3xx] Scoring**

ErrorCode | ErrorDescriptión | ErrorDetail
---------|----------|-----------------------
340 | Account without scoring
342 | Scoring amount per transaction out-of-range 
343 | Scoring amount per transaction out-of-range
344 | Amount of daily scoring out-of-range
345 | Monthly scoring amount per transaction out-of-range 
346 | Scoring amount per transaction out-of-range 

**[3xx] SSO and Payment Errors**

ErrorCode | ErrorDescriptión | ErrorDetail
---------|----------|-----------------------
350 | Missing Account or User Account
351 | Invalid external ID
352 | Missing account tax code
353 | Missing account business name
354 | Duplicate username or email
355 | Missing client transaction id
356 | Missing payment intent
357 | Missing provider payment intent
358 | Missing provider id payment intent
359 | Missing provider business name payment intent
360 | Missing customer payment intent
361 | Missing customer identification number payment intent
362 | Missing customer provider identification number payment intent

**[3xx] Generic override error**

ErrorCode | ErrorDescriptión | ErrorDetail
---------|----------|-----------------------
399 | Generic

**[4xx] Errors involving feasible delays of subsequent queries of the state**

ErrorCode | ErrorDescriptión | ErrorDetail
---------|----------|-----------------------
401 | Transaction status | Transaction in process.
402 | Transaction status | Transaction cancelled.
403 | Transaction status | Transaction pending confirmation.
404 | Transaction status | Transaction in process.

**[5xx] Action-type or validation errors**

ErrorCode | ErrorDescriptión | ErrorDetail
---------|----------|-----------------------
501 | Action / validation | Transaction not confirmed.
502 | Action / validation | Incorrect account type.
503 | Action / validation | Incorrect price list.
504 | Action / validation | User code in use.
505 | Action / validation | Incorrect user code.
506 | Action / validation | Type incorrect claim.
507 | Action / validation | Wrong date.
508 | Action / validation | Incorrect configuration.
509 | Action / validation | Wrong time.

**[6xx] Generic type errors**

ErrorCode | ErrorDescriptión | ErrorDetail
---------|----------|-----------------------
601 | Incorrect validations | Incorrect validations.

**Card Module**

ErrorCode | ErrorDescriptión | ErrorDetail
---------|----------|-----------------------
147001 | Non-operational card account to transact
147002 | Non-operational card for transacting

