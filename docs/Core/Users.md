## Users

El presente documento describe el uso de las APIs para poder realizar distintos tipos de acciones relacionadas a Usuarios.

Mediante estos métodos se podrá obtener un Token para utilizar el resto de los métodos, registrar un nuevo usuario/cuenta en la Plataforma, obtener sus datos y actualizar los mismos según sea necesario.

## Register

Mediante el método “**Register**” (**POST - /account/finalConsumerAccount/register**) se dan de alta cuentas en la Plataforma OP.

El registro de cuentas se complementa con todas sus alternativas de agregado y modificación de datos mediante una serie de métodos asociados en caso de que se quiera realizar una carga de información del usuario de manera progresiva, es decir, realizar un alta de cuenta inicial con los datos mínimos y luego, según el cliente vaya necesitando/queriendo, irá agregando el resto de sus datos nominativos.

Por ej: email, phone, tributary-info, etc.

El método **Register** es el método principal de la Plataforma OpenPass ya que el resto de los métodos y funciones de la plataforma necesitan contar con una cuenta creada en la Plataforma.

 > **NOTA:** Siempre que el método “Register” devuelva un **OK**, significa que la cuenta **YA** fue creada en la Plataforma OP, con lo cual, en caso de volver a llamar al método con los mismos parámetros, el proceso devolverá un error por “Cuenta Existente”

Si bien la Plataforma OP permite manejar un ID de Cliente Externo (sería el ID mediante el cual quien consume los servicios de OP identifica sus cuentas), se recomienda de todas maneras persistir el ID de Cuenta de OP con el objetivo de mantener una mejor sincronización de datos entre plataformas, permitir un mejor troubleshooting, etc.

![](../../assets/images/proceso_register.png)
