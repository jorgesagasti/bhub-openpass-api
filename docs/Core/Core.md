# Core

La Plataforma de Core Fintech, Cash Management & Billetera Móvil White Label de OpenPass proporciona los medios para desarrollar una Experiencia de Pago Digital punta a punta y a su vez libera a los servicios financieros de sus limitaciones tradicionales al haber hecho ya todos los trabajos pesados por usted, al permitir la configuración e implementación rápida y asequible de la banca digital, la infraestructura de pagos, y soluciones de tarjetas, entre otras.

Tal como mencionamos anteriormente, OpenPass brinda un enfoque modular mediante el cual los clientes pueden aprovechar una pila de tecnología completa o sus componentes: interfaces UX / UI de marca blanca, servicios de middleware y orquestación, core fintech y emisión, adquisición y procesamiento de transacciones de tarjetas, etc.

Dentro del módulo CORE se encuentran las funcionalidades principales de la Plataforma las cuales son fundamentales y necesarias para poder utilizar el resto de las funcionalidades.

Las siguientes son las principales funcionalidades consideradas como Core dentro de la amplia variedad de módulos de la Plataforma:

- Users.

- Cuentas.

- Financial Accounts.

- Transferencias.

- Promociones.

- Treasury/Cashback.

# _Core_

_OpenPass' Core Fintech, Cash Management & White Label Mobile Wallet Platform provides the means to develop an end-to-end digital payment experience. It also frees financial services from their traditional limitations as OpenPass has already done the hard work, thus, enabling a fast and affordable setup and deployment of digital banking, payment infrastructure, and card solution, among others._

_OpenPass provides a modular approach whereby customers can leverage an entire technology stack or its components individually: white-label UX/UI interfaces, middleware and orchestration services, core fintech and issuance, acquisition and processing of card transaction, etc.
Within the CORE module are the main functionalities of the Platform which are fundamental and necessary to use the other features._

_These are the main functionalities considered as Core within the wide range of modules of the Platform:_


- _Users._

- _Accounts._

- _Financial Accounts._

- _Transferences._

- _Promotions._

- _Treasury/Cashback._
