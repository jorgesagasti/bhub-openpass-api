---
stoplight-id: e96e00c2b2047
---

**EXTENSIBILIDAD BHUB**

- **Transacciones Comerciales Genéricas -**



1. **Objetivo**

El objetivo de esta feature es ampliar las capacidades de BHUB dentro del concepto de “extensibilidad” el cual tiene como objetivo, brindar a quien se integre (en adelante “el Cliente”) con BHUB la posibilidad de  crear nuevos tipos de transacciones, además de las que ya maneja BHUB, las cuales sean capaces de modificar el stock de un producto determinado dentro de las Cuentas Custodia de BHUB utilizando métodos genéricos creados a tal fin. 

Con esto, quien se integre con BHUB, tendrá la independencia de agregar nuevos productos y/o funcionalidades, ya sea desarrollándolo in-house o integrándose con proveedores nuevos o existentes, siendo el cliente, owner de la conectividad y/o integración contra los mismos y la construcción de dicho producto y/o funcionalidad.

1. **Detalle**

Este punto se refiere a facilitar la integración con un proveedor externo a BHUB el cual no se encuentre previamente integrado.

La plataforma BHUB se soporta en 4 pilares principales, Cuentas, Productos, Stocks y Transacciones. 

La extensibilidad del producto vendrá dada por la incorporación del concepto de Transacciones Comerciales Genéricas las cuales serán soportadas por Producto, Stock y Transacciones.


**Transacciones Comerciales Genéricas**

Las transacciones comerciales genéricas tendrán la característica de no necesitar estar modeladas previamente dentro de la plataforma de forma tal de dar la posibilidad al cliente de Gestionar y/o Construir cualquier producto no incluido de manera predefinida en BHUB utilizando a BHUB como herramienta de Cash Management y/o Core Financiero/Transaccional.

El comportamiento de las mismas tendrá una ligera diferencia respecto a las transacciones predefinidas o existentes las cuales se detallan a continuación.

**Core de Transacciones existentes**

Llamamos core de transacciones existentes a todas aquellas transacciones disponibles a través de métodos públicos y disponibles en la plataforma las cuales impactan sobre los 4 pilares antes mencionados. Por ej, alguna de estas transacciones pueden ser de los siguientes tipos:

- Transferencias Bancarias Cash Out /Cash In
- Transferencias no Bancarias entre usuarios de la plataforma
- Transacciones de Consumo Tarjeta PrePaga,
- Transacciones de Cash In / Cash Out de Efectivo
- Pago y cobro de Facturas y servicios
- Recargas Telefonía celular
- Recargas Transporte
- Recargas TV
- Transacciones de Pagos por Medio de códigos QR
- Transacciones de Reintegro
- Transacciones de Ajuste Notas de crédito y Débito

Las transacciones Core detalladas con anterioridad corresponden a transacciones de Productos predefinidos y modelados dentro de la plataforma, del mismo modo que se encuentra modelada y gestionada la conectividad con los proveedores de los mismos.


**Lógica de negocio transacción comercial genérica**

La lógica de negocio de la transacción comercial genérica será administrada **por fuera del motor transaccional de BHUB**, esto dará la posibilidad al cliente de definir el flujo, comportamiento, validaciones, etc de cada transacción conforme a la necesidad del negocio. 

Del mismo modo, al estar la lógica de negocio de las nuevas transacciones comerciales por fuera de BHUB, las validaciones de negocio, aprobación y/o rechazo, comportamientos, etc serán realizadas por el cliente desde su backend.

Una vez determinado que la transacción es válida y pertinente, se enviará la misma a BHUB para su registro utilizando los métodos de transacciones genéricas.

Las transacciones genéricas pueden ingresar o extraer dinero de la cuenta (Crédito / Débito)

**Por ej:**

Supongamos que el Cliente desea incorporar una nueva funcionalidad a su producto para la cual es necesario integrarse con un proveedor con quien BHUB no tiene integración y el Cliente decide integrar directamente su backend con dicho proveedor.

El Cliente realizaría las integraciones necesarias, definiría el flujo entre su producto y dicho proveedor, y una vez que el flujo termine y/o sea resuelta una transacción financiera/económica o de algún otro tipo que deba quedar registrada en la Cta.Cte del Usuario, el Cliente la informará a BHUB mediante la API de Transacciones Genéricas con la información de la misma para su registros en BHUB.

**Esquema**

El siguiente esquema muestra un ejemplo de arquitectura posible, a muy alto nivel, de un Cliente que se integre con BHUB tanto para transacciones existentes como transacciones genéricas.

![](../../assets/images/extensibilidad/Aspose.Words.58cd8863-a24f-43e0-be5d-d51d9f0479d4.002.png)





**Diagrama de la Solución**

Las solución de Transacciones Comerciales Genéricas puede utilizarse mediante dos esquemas posibles: 

- Esquema de Solicitud y Advice
- Esquema de Transacción Directa.

*Esquema de Solicitud y Advice*

El siguiente diagrama muestra la interacción entre el Cliente, BHUB y el Proveedor externo para el esquema de Solicitud y Advice.

![](../../assets/images/extensibilidad/Aspose.Words.58cd8863-a24f-43e0-be5d-d51d9f0479d4.003.png)

**Descripción del Flujo**

1. El cliente inicia una Solicitud de Transacción Genérica en BHUB con el atributo ***useAdviceMode*** en true.
1. Esto le indica a BHUB que el cliente va a iniciar una TRX con un proveedor en la modalidad Solicitud-Advice (es decir, inicia una transacción que luego va a terminar de confirmar con un estado final ya sea como Approved o Rejected).
1. BHUB realiza las validaciones correspondientes y si todo esta OK, devuelve un 200, deja la TRX en Status: “Pending” y devuelve un Nº de TRX
1. BHUB queda a la espera de un nuevo llamado mediante el cual la TRX se cierra (se informa el estado Final de la TRX).
1. En el medio, el Cliente interactúa con el Proveedor, de la manera que el Cliente/Proveedor definan.
1. El Cliente deberá realizar las integraciones, validaciones, procesamiento, lógica, y todo lo necesario para procesar dicha transacción.
1. Luego de completar todo el ciclo entre Cliente y Proveedor, el Cliente envía a BHUB un "Advise de Solicitud" donde finaliza la TRX iniciada con la Solicitud de Transacción Genérica. Mediante este Advice, le informa a BHUB el estado final de la TRX (Approved, Rejected).
1. Dentro de este proceso, BHUB contará con un Time-Out configurable para poder cerrar la Solicitud de Transacción Genérica en caso de que el Cliente NO logre completar el proceso o no logre enviar el Advice.

*Esquema de Transacción Directa*

El siguiente diagrama muestra la interacción entre el Cliente, BHUB y el Proveedor externo para el esquema de Transacción Directa.

![](../../assets/images/extensibilidad/Aspose.Words.58cd8863-a24f-43e0-be5d-d51d9f0479d4.004.png)

**Descripción del Flujo**

1. El cliente envía un llamado a un método de Transacción Genérica en BHUB con el atributo ***useAdviceMode*** en false.
1. Esto le indica a BHUB que el cliente quiere generar una Transacción Genérica de manera Directa, es decir, que impacte directo en BHUB SIN esperar una confirmación (Advice) por parte del cliente. 
1. BHUB realiza las validaciones correspondientes y si todo está OK, devuelve un 200, deja la TRX en Status: “Approved o “Rejected” y devuelve un Nº de TRX.

Al utilizar este flujo, BHUB entiende que el Cliente ya realizó las validaciones previas y/o lo que corresponda y que la Transacción que envía ya tiene un estado final de resolución de la misma y solamente necesitar registrarla en el Core de BHUB.

**Endpoints**

A continuación se detalla la lista de métodos a utilizar en los flujos antes descriptos.

- {{baseUrl}}/generic-transaction/**credit**
- {{baseUrl}}/generic-transaction/**debit**
- {{baseUrl}}/generic-transaction/**{{genericTransactionId}}**/**advice**

**{{genericTransactionId}}:** se obtiene de la Solicitud de Transacción Genérica

**Estados**

Como se detalló anteriormente, las transacciones genéricas podrán tener 3 estados diferentes:  “*Approved*”, “*Rejected*” o “*Pending*”.

En la modalidad Solicitud-Advice, la transacción se iniciará con un estado *Pending* el cual deberá ser modificado a un estado Final al terminar el proceso entre el Cliente y el Proveedor o cumplirse el time-out definido.

En la modalidad Transacción Directa, la transacción se generará con un estado Final (*Approved* o *Rejected*). Se asume que el Cliente realiza el proceso y/o validaciones necesarias previo a la generación de la Transacción Genérica.

En caso de que por algún motivo el Cliente no lograra cerrar el flujo con el Proveedor, o por algún motivo necesite validar el estado Final de una transacción, podrá consultar el mismo mediante los métodos actuales de Consulta de Transacción.
**


**Validaciones de consistencia:**

BHUB realizará las validaciones correspondientes tanto en la Solicitud de Transacción Genérica como en el Advise de Solicitud. Dichas validaciones serán las mínimas necesarias para garantizar el correcto registro de la misma. Como ser por ej.:

- Cuenta destino de la transacción existente
- Cuenta destino en un estado apto para recibir transacciones
- Saldo, Scoring, etc

La posibilidad de enviar transacciones en estado “*Rejected*” se ofrecen de manera tal de tener la información completa del lado de BHUB, para poder tener del lado de plataforma la información de todas las transacciones de una cuenta puesto que puede ser objeto de análisis de las áreas de Fraude y Prevención de lavado de activos, Financieras, Operaciones, etc.



**Reversas / Devolución**

Las transacciones genéricas son de Crédito y Débito, esto indicará de qué manera debe impactar en el Stock del producto asociado a dicha transacción.

En caso de necesitar realizar una Reversa/Devolución se deberá generar una transacción opuesta a la transacción originante.  Las reversas/devoluciones podrán ser Totales o Parciales.

**Stocks**

El uso de transacciones genéricas prevé un único medio de entrada y salida de los stocks administrados dentro de BHUB. Esto en otras palabras, significa que al usar transacciones genéricas BHUB asegurará el movimiento del producto en las cuentas de las billeteras de forma controlada y validada.

Por el momento, las Transacciones Comerciales Genéricas impactan sobre el Saldo de la Cuenta Billetera por default. No se puede seleccionar de que cuenta acreditar/debitar el dinero, por el momento sólo del stock de la cuenta default.



**Consultas – Recuperación – Conciliación**

El posteo de las transacciones deberá ser tratado de forma análoga a las existentes, esto quiere decir que en caso que no se haya recibido respuesta por parte del backend de BHUB se deberá proceder a recuperar el estado a través de la consulta. Caso contrario, en caso de recibir una respuesta la misma podrá ser tomada como una respuesta final.

Sea tanto en pos de realizar una conciliación o por el solo hecho de conocer el estado o el detalle de la transacción, el modo de consulta para la transacción genérica será del mismo modo que se hacen las consultas a las transacciones existentes, es decir que se podrá recuperar el estado de una transacción a través de un método GET a partir del Identificador de transacción original.


**Sincronización Cliente-BHUB**

Dado que en las transacciones genéricas participan 3 actores diferentes (**BHUB**, **Cliente** integrado a BHUB, y **Proveedor** integrado a Cliente) , es importante que se prevea un mecanismo de sincronización entre los 3 para asegurar un correcto estado de cada transacción en la persistencia de cada uno de los actores.

Este mecanismo debe considerar la posibilidad de que fallen las comunicaciones o cualquier otro inconveniente que cause una diferencia de estado de transacción en alguno de los 3 actores y deberá asegurar la sincronización en base a los estados finales de cada transacción.

Principalmente en la modalidad Solicitud-Advice donde el proceso con BHUB es asincrónico, deberá contemplar que la transacción pueda quedar en estado “*Pending*”, que pueda quedar en estado “*Rejected*” debido al vencimiento del time-out, etc.


**Hooks – Eventos**

Las transacciones genéricas tendrán predefinido generar los eventos en similares condiciones a los existentes acorde a los estados posibles. 

Para recibir los eventos se deberá suscribir de igual forma que el resto de las transacciones mediante el ***trasactionType*** code.



**Particularidades de las Transacciones Genéricas *- Request -***

- **useAdviceMode:**

Tal como mencionamos anteriormente, este atributo booleano, le indica a BHUB si la transacción genérica es del modo Solicitud-Advise o del modo Transacción Directa.

En la sección Diagrama de Flujo se puede ver el funcionamiento en cada caso.

- **transactionType**:

Las transacciones genéricas pueden utilizarse para cualquier propósito por ende es importante poder segmentar los diferentes tipos de transacciones y poder luego identificar a qué corresponde cada una.

Eso se logra mediante el uso del atributo ***transactionType***. Previamente deben crearse los Tipos de Transacciones desde el RUI para luego poder ser utilizado dicho valor en las transacciones genéricas.

- **stateDetail:**

El atributo ***stateDetail*** permite informar el Código de Respuesta asociado a la transacción genérica. 

Cabe aclarar que estos Códigos DEBEN ser los utilizados por BHUB en su Tabla de Códigos de Error. 

- **transaction Data:** 

Como toda transacción en BHUB, las Genéricas también cuenta con un objeto ***transactionData*** el cual contiene información relacionada a la transacción. Dicha información se encuentra segmentada en 2 objetos:

- **commercialData**

Dado que las transacciones son genéricas y tal como mencionamos anteriormente no forman parte del grupo de transacciones modeladas en BHUB, el contenido del objeto ***commercialData*** en para que el Cliente pueda asociar cualquier tipo de información a la transacción que necesite . 

Cabe aclarar que BHUB no realiza ningún tipo de control, validación, procesamiento, 

**NOTA**: el contenido de ***transactionData*** debe ser un JSON.

- **uiData**

Adicionalmente a lo detallado previamente, en las Transacciones Comerciales Genéricas se incorpora un nuevo objeto, el UI Data.

Este objeto permite configurar la pantalla del BackOffice donde se consultan los detalles de una transacción y poder así definir qué campo y que valores se van a mostrar en dicha pantalla.

Al ser transacciones genéricas y no formar parte de las transacciones modeladas en BHUB, el contenido e información relacionada a las mismas puede ser variable según el producto, negocio y/o funcionalidad asociada a la transacción y por ende la información a visualizar es propia del tipo de transacción.

En el **Anexo I** se puede encontrar más detalle sobre este atributo en particular.


- **forceTransaction:** 

Este es un atributo muy importante y potente dentro de la solución de Transacciones Genéricas y debe ser utilizado con ***extrema precaución***.

La función de este atributo es permitir insertar transacciones genéricas **BYPASEANDO** las validaciones que realiza BHUB sobre las transacciones (salvo las validaciones mínimas, como por ej: que la cuenta exista, que esté en estado operable, etc). 

Es muy importante utilizar este atributo en los casos donde realmente sea necesario bypasear las validaciones porque se pueden generar graves problemas.

Si el valor es ***true***, bypasea las validaciones y si es ***false*** las realiza de manera usual.

**Riesgo**: al bypasear las validaciones, la cuenta podría quedar en saldo negativo ya que no se controla por ejemplo el saldo en una operación de Débito, es decir, se le va a debitar el importe solicitado aun cuando la cuenta pueda no tener saldo suficiente. 



**Ejemplo de Request: (Crédito/Débito)**

{

`   `"amount":2,

`   `"clientTransactionId":"{{clientTransactionId}}",

`   `"forceTransaction":**false**,

`   `"useAdviceMode":**false**,

`   `"transactionType":"PaymentProvider",

`   `"state":"rejected",

`   `"stateDetail":{

`      `"statusResponseCode":315

`   `},

`   `"transactionData":{

`      `"commercialData":{

`         `"MiproductoKey":"miproductoDescription"

`      `},

`      `"uiData":{

`          `"properties":[

`               `{

`                  `"controlProperties":{

`                      `"label": "campoSimple"                        

`                  `},

`                  `"fieldProperties":{

`                      `"value": "valorSimple"

`                  `}

`              `},

`              `{

`                  `"controlProperties":{

`                      `"label": "campoNegrita"                        

`                  `},

`                  `"fieldProperties":{

`                      `"value": "Esto es un valor con negrita.",

`                      `"style": "font-weight: bolder;font-size: 15px;"

`                  `}

`              `},

`              `{

`                  `"controlProperties":{

`                      `"label": "Monto",

`                      `"addOn": "$"                        

`                  `},

`                  `"fieldProperties":{

`                      `"value": "124124"

`                  `}

`              `},

`              `{

`                  `"controlProperties":{

`                      `"label": "Paypal Email",

`                      `"addOnImage": "fa fa-paypal"                        

`                  `},

`                  `"fieldProperties":{

`                      `"value": "mbpaypal@gmail.com",

`                      `"class": "header-stat purple  pull-right"

`                  `}

`              `},

`              `{

`                  `"controlProperties":{

`                      `"label": "Atencion",

`                      `"addOnImage": "fa fa-exclamation"                        

`                  `},

`                  `"fieldProperties":{

`                      `"class": "note note-warning", 

`                      `"value": "Requiere confirmacion."

`                  `}

`              `}



`          `]                  

`      `}

`   `}

}



**Particularidades de las Transacciones Genéricas *- Response -***

El response devuelve en líneas generales los mismos datos de la Transacción con algunos agregados propios del procesamiento de la misma.*** 

- **className:** Indica que es un tipo de Transacción Genérica

- **entryType:** Indica si es una Transacción de Crédito o Débito

- **state:** Indica el estado de la Transacción:  “*Approved*”, “*Rejected*” o “*Pending*”** 

- **stateDetail:**

El atributo ***stateDetail*** devuelve el Código de Respuesta asociado a la transacción genérica, el Tipo de Transacción (***transactionType***).

Cabe aclarar que estos Códigos son los de BHUB según su Tabla de Códigos de Error. 

- **transactionType:** Indica el tipo de transacción. 

**Ejemplo de Response:**

{

`    `"id": "253D03C70483AE093A45E9AE00525719B4A2",

`    `"className": "GenericTransaction",

`    `"number": 1328249,

`    `"entryType": "Credit",

`    `"created": "2022-08-25T18:02:18-03:00",

`    `"businessDate": "20220825",

`    `"confirmationDate": "2022-08-25T18:02:18-03:00",

`    `"amount": 2.00,

`    `"description": "Pago Proveedor - 1328249",

`    `"state": "approved",

`    `"stateDetail": {

`        `"message": "Confirmada",

`        `"transactionType": "PaymentProvider",

`        `"statusResponseCode": {

`            `"code": 315,

`            `"description": "Sucursal incorrecta"

`        `}

`    `},

`    `"transactionType": "PaymentProvider",

`    `"forceTransaction": **false**,

`    `"clientTransactionId": "887556d5-c20e-4f0f-9a3f-c1c4de03eca3",

`    `"clientAdditionalData": {},

`    `"useAdviceMode": **false**,

`    `"transactionData": {

`        `"id": "7B9D03C70483AE0D6002891900528130C4B9",

`        `"className": "GenericCommercialData",

`        `"commercialData": {

`            `"MiproductoKey": "miproductoDescription"

`        `},

`        `"uiData": {

`            `"properties": [

`                `{

`                    `"fieldProperties": {

`                        `"value": "valorSimple"

`                    `},

`                    `"controlProperties": {

`                        `"label": "campoSimple"

`                    `}

`                `},

`                `{

`                    `"fieldProperties": {

`                        `"style": "font-weight: bolder;font-size: 15px;",

`                        `"value": "Esto es un valor con negrita."

`                    `},

`                    `"controlProperties": {

`                        `"label": "campoNegrita"

`                    `}

`                `},

`                `{

`                    `"fieldProperties": {

`                        `"value": "124124"

`                    `},

`                    `"controlProperties": {

`                        `"addOn": "$",

`                        `"label": "Monto"

`                    `}

`                `},

`                `{

`                    `"fieldProperties": {

`                        `"class": "header-stat purple  pull-right",

`                        `"value": "mbpaypal@gmail.com"

`                    `},

`                    `"controlProperties": {

`                        `"label": "Paypal Email",

`                        `"addOnImage": "fa fa-paypal"

`                    `}

`                `},

`                `{

`                    `"fieldProperties": {

`                        `"class": "note note-warning",

`                        `"value": "Requiere confirmacion."

`                    `},

`                    `"controlProperties": {

`                        `"label": "Atencion",

`                        `"addOnImage": "fa fa-exclamation"

`                    `}

`                `}

`            `]

`        `}

`    `}

}

**Particularidades de las Transacciones Genéricas *- Método Advice -***

El método Advice de la modalidad Solicitud-Advice, tiene las siguientes particularidades.

- {{baseUrl}}/generic-transaction/**{{genericTransactionId}}**/**advice**

El atributo **{{genericTransactionId}}** es el ID de la transacción devuelto por la Solicitud de Transacción Genérica (Credito o Debito).

No es necesario enviar todo el objeto transaction, sino que solamente se deberá enviar el estado final en el cual se desea setear la transacción. Este estado puede ser “*Approved*” o “*Rejected*” y la transacción debe encontrarse en estado “*Pending*”.

En el caso de que la transacción no se encuentre en estado “*Pending*”* ya sea porque BHBU devolvió timeout en un primer intento o por cualquier otro motivo la transacción se encuentre en un estado diferente a Pending pueden ocurrir 2 situaciones:

- Que el estado sea el mismo: la respuesta será exitosa y confirmará el estado
- Que el estado sea diferente: en cuyo caso devolverá error.

El método Advice contempla la posibilidad de agregar más información a la transacción, como por ej: un ***Confirmation\_ID***, ***ClientAditionalData***, ***Código de Respuesta de Error***, ***Motivo de Rechazo***, etc.

De esta manera se logra enriquecer más la información de la transacción genérica con mayor detalle obtenido luego de completar el proceso entre el Cliente y el Proveedor antes de cerrar la transacción genérica.




**Reportes e interfaces de observabilidad**

Las transacciones genéricas podrán ser incluidas dentro de los reportes e interfaces utilizadas por el Cliente, solamente se deberán solicitar incluirlas en los mismos.

Las transacciones genéricas podrán tener un apartado de atributos propios del negocio o producto generados y administrados por el Cliente según lo requiera. 

BHUB solo asociará dicha información a la transacción persistiendo la misma para el uso exclusivo del Cliente. Es decir, BHUB no realizará ningún análisis, uso, procesamiento ni nada relacionado con dicha información. Estos datos enriquecidos no tienen ni tendrán flujos ni lógica dentro de la plataforma.



1. **Fuera de Alcance**

Queda fuera de alcance el acuerdo de conectividad con los diferentes vendors y/o proveedores intervinientes de los flujos transaccionales fuera de BHUB. Es decir que el establecimiento de la conexión con los mismos quedará circunscripto entre el backend del Cliente y el proveedor. En otras palabras el flujo contemplado en este proyecto incluye comunicación entre el Cliente y BHUB.

Del mismo modo que el caso anterior no se prevé mecanismos de conciliación externos como archivos, etc.

Queda también fuera del alcance de esta solución como así también de la Plataforma BHUB, la lógica de negocio de la transacción comercial genérica, el flujo, comportamiento, validaciones, etc de cada transacción. Todo esto será administrado por fuera del motor transaccional de BHUB.




**Anexo I - UI Data**

Las transacciones Genéricas tienen un campo específico para mostrar sus datos en BHUB BackOffice, ese campo se llama ***uiData***:

Este objeto permite configurar la pantalla del BackOffice donde se consultan los detalles de una transacción y poder así definir qué campo y que valores se van a mostrar en dicha pantalla.

Al ser transacciones genéricas y no formar parte de las transacciones modeladas en BHUB, el contenido e información relacionada a las mismas puede ser variable según el producto, negocio y/o funcionalidad asociada a la transacción y por ende la información a visualizar es propia del tipo de transacción.

**Descripción**:

Cada componente de la UI se dibuja en 2 partes:

- el componente de control 
- el componente de campo

![](../../assets/images/extensibilidad/Aspose.Words.58cd8863-a24f-43e0-be5d-d51d9f0479d4.005.png)

Es una colección de objetos con ***controlProperties*** y ***fieldProperties*** 

La manera más simple de usarlo es asignar un label en ***controlProperties***  y un value en ***fieldProperties***.

Ejemplo: 

"uiData":{

`          `"properties":[

`               `{

`                  `"controlProperties":{

`                      `"label": "campoSimple"                        

`                  `},

`                  `"fieldProperties":{

`                      `"value": "valorSimple"

`                  `}

`              `},



También se pueden incorporar elementos de personalización, como por ejemplo los ***addOn***, que pueden ser tanto un String como una Imagen de la librería de UI que usamos (Metronic:[](https://preview.keenthemes.com/metronic-v4/theme/admin_4/ui_icons.html)

[](https://preview.keenthemes.com/metronic-v4/theme/admin_4/ui_icons.html)[Metronic Admin Theme #4 | Font Icons](https://preview.keenthemes.com/metronic-v4/theme/admin_4/ui_icons.html) ) 

Ejemplo:

{

`      `"controlProperties":{

`           `"label": "Monto",

`           `"addOn": "$"                        

`       `},

`       `"fieldProperties":{

`           `"value": "124124"

`       `}

},

{

`       `"controlProperties":{

`            `"label": "Paypal Email",

`            `"addOnImage": "fa fa-paypal"                        

`        `},

`       `"fieldProperties":{

`            `"value": "mbpaypal@gmail.com",

`            `"class": "header-stat purple  pull-right"

`        `}

},

![](../../assets/images/extensibilidad/Aspose.Words.58cd8863-a24f-43e0-be5d-d51d9f0479d4.006.png)


Otra posibilidad es mandarle cualquier personalización que entienda el field, como por ejemplo style o class, brindando la posibilidad de editar el estilo ***css*** directamente o utilizando alguna de las clases de componentes de nuestras librerías.

Ejemplo:

{

`        `"controlProperties":{

`               `"label": "campoNegrita"                        

`         `},

`        `"fieldProperties":{

`               `"value": "Esto es un valor con negrita.",

`               `"style": "font-weight: bolder;font-size: 15px;"

`        `}

},

