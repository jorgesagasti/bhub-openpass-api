# Introducción

#### Casos de uso:

- [Registro de Usuario de Billetera](docs/casos_de_uso/10_registro_de_usuario_de_billetera.md)
- [Recargas](docs/casos_de_uso/20_recargas.md)
- [Pago de Servicios](docs/casos_de_uso/30_pago_de_facturas.md)
- [Cuentas de Ahorro](docs/casos_de_uso/40_cuentas_de_ahorro.md)
- [Tarjetas](docs/casos_de_uso/50_tarjetas.md)
- [Tratamiento de Errores](docs/casos_de_uso/90_tratamiento_de_errores.md)
- [Servicios Bancarios](docs/casos_de_uso/92_servicios_bancarios.md)
